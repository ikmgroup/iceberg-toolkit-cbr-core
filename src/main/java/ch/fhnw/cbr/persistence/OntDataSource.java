/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.fhnw.cbr.persistence;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;

/**
 * Encapsulates the ontology models of the CBR component:
 *   - schema model: All the models defining the domain model (T-Box)
 *   - data model: Runtime data model with inferenced and/or asserted instances during the application runtime.
 *   - model: A combined model of both schema and data.
 * 
 * The models are Jena models that are known to contain ontology data, under a given ontology vocabulary (such as OWL).
 * 
 * @author sandro.emmenegger
 */
abstract public class OntDataSource {

    private OntModel schemaModel;
    private OntModel dataModel;
    
    /**
     * Using the ModelFactory.createUnion function has 2 advantages: 
     *   1. Unified model is dynamic and not a copy of the underlying models
     *   2. All new instances are stored automaticly in the dataModel only
     */
    private OntModel model;
    
    protected OntDataSource(){};
    
    public abstract void persistData();
    
    public OntDataSource(OntModel schemaModel, OntModel dataModel){
        load(schemaModel, dataModel);
    }

    public OntModel getSchemaModel(){
        return schemaModel;
    }
    
    public OntModel getDataModel(){
        return dataModel;
    }
    
    public OntModel getModel(){
        return model;
    }
    
    protected void load(OntModel schemaModel, OntModel dataModel){
        this.schemaModel = schemaModel == null ? ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM) : schemaModel;
        this.dataModel = dataModel == null ? ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM) : dataModel;
        model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, ModelFactory.createUnion(dataModel, schemaModel));   
    }
}
