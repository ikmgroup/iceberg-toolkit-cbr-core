/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.fhnw.cbr.persistence;

import ch.fhnw.cbr.core.config.CBR;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;

/**
 * Loads schema and data models from source files defined in the components
 * configuration properties.
 *
 * @author sandro.emmenegger
 */
public class DefaultOntDataSource extends OntDataSource {

    private static final Lang TTL_FILE_FORMAT = Lang.TTL;
    private static final String TTL_FILE_ENDING = ".ttl";

    private static final Log LOGGER = LogFactory.getLog(DefaultOntDataSource.class);

    public DefaultOntDataSource() {
        loadModels();
    }

    public DefaultOntDataSource(OntModel schemaModel, OntModel dataModel) {
        super(schemaModel, dataModel);
    }

    private void loadModels() {
        OntModel schemaModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
        schemaModel.getDocumentManager().setProcessImports(false);

        OntModel dataModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
        dataModel.getDocumentManager().setProcessImports(false);

        String[] paths = CBR.CONFIG.getStringArray("ontology.source");
        if (paths == null) {
            LOGGER.warn("No ontology schema source defined in default loader of CBR component.");
        } else {
            for (String path : paths) {
                addModel(schemaModel, path);
            }
        }

        //Load data model
        String dataPath = CBR.CONFIG.getString("ontology.data.file");
        if (dataPath == null) {
            LOGGER.warn("No ontology data file source defined in default loader of CBR component.");
        } else {
            File dataFileForSave = new File(CBR.CONFIG.getString("ontology.data.file"));
            if (!dataFileForSave.exists()) {
                InputStream in = DefaultOntDataSource.class.getResourceAsStream(CBR.CONFIG.getString("ontology.data.file"));
                if (in != null) {
                    dataModel.read(in, null, TTL_FILE_FORMAT.getName());
                } else {
                    LOGGER.warn("Cannot load ontology data file. Data file does not exist: " + dataFileForSave);
                }
            } else {
                dataModel.read(dataPath, TTL_FILE_FORMAT.getName());
            }
        }
        load(schemaModel, dataModel);
    }

    private void addModel(OntModel model, String directoryOrFilePath) {
        InputStream in = null;
        try {
            //Try first to load from classpath
            in = DefaultOntDataSource.class.getResourceAsStream(directoryOrFilePath);
            if (in != null && directoryOrFilePath.endsWith(TTL_FILE_ENDING)) {
                model.read(in, null, TTL_FILE_FORMAT.getName());
            } else {
                //Try to load from directory or file in filesystem
                File file = new File(directoryOrFilePath);
                if (file.isDirectory()) {
                    File[] files = file.listFiles(new FileFilter() {
                        @Override
                        public boolean accept(File pathname) {
                            return pathname.toString().endsWith(TTL_FILE_ENDING);
                        }
                    });
                    for (File f : files) {
                        addModel(model, f.toString());
                    }
                } else if (directoryOrFilePath.endsWith(TTL_FILE_ENDING)) {
                    in = new FileInputStream(directoryOrFilePath);
                    model.read(in, null, TTL_FILE_FORMAT.getName());
                }
            }
        } catch (FileNotFoundException ex) {
            LOGGER.warn("Cannot load ontology schema file. File does not exist: " + directoryOrFilePath);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    LOGGER.error("Failed to close input stream during ontology schema loading of file: " + directoryOrFilePath);
                }
            }
        }

    }

    /**
     * Persist complete data model including all changes (asserted instances and
     * properties). File for data can be configured.
     */
    @Override
    public void persistData() {
        String dataFilePath = CBR.CONFIG.getString("ontology.data.file");
        if (dataFilePath == null) {
            LOGGER.warn("Cannot persist data model. The configuration property 'ontology.data.file' is not set.");
            return;
        }

        File dataFile = new File(dataFilePath);
        if (dataFile.exists()) {
            try (FileOutputStream fileOut = new FileOutputStream(dataFile)) {
                RDFDataMgr.write(fileOut, getDataModel(), RDFFormat.TURTLE_BLOCKS);
            } catch (IOException ex) {
                LOGGER.error("Error during save of instances to data file. ", ex);
            }
        } else {
            LOGGER.warn("Cannot persist data model. Data file does not exist: " + dataFilePath);
        }
    }
}
