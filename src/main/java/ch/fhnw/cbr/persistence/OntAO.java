/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.persistence;

import ch.fhnw.cbr.core.config.CBR;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Provides a convenient access to the loaded ontology.
 *
 * @author sandro.emmenegger
 */
public class OntAO {
    
    private static final Log LOGGER = LogFactory.getLog(OntAO.class);

    private static final OntAO SINGLETON = new OntAO();

    /**
     * Encapsulated ontology models *
     */
    private OntDataSource dataSource;

    /**
     * Load ontology dependent on the persistence configuration (properties
     * file).
     */
    private OntAO() {
        dataSource = new DefaultOntDataSource();
    }

    public static OntAO getInstance() {
        return SINGLETON;
    }
    
    public synchronized void setOntDataSource(OntDataSource ontDataSource){
        this.dataSource = ontDataSource;
    }

    public OntModel getOntModel() {
        return dataSource.getModel();
    }

    public OntModel getOntDataModel() {
        return dataSource.getDataModel();
    }
    
    public OntModel getOntSchemaModel(){
        return dataSource.getSchemaModel();
    }

    public OntClass getOntClass(String uri) {
        return dataSource.getModel().getOntClass(uri);
    }

    public OntProperty getOntProperty(String uri) {
        return dataSource.getModel().getOntProperty(uri);
    }



    /**
     * Returns all instances of the given OntClass including instances of
     * subclasses.
     *
     * @param rootClass
     * @return
     */
    public List<Individual> getInstances(OntClass rootClass) {
        List<Individual> retInstances = new ArrayList<>();
        if (rootClass.getURI() == null) {
            return retInstances;
        }

        String queryString
                = "PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "SELECT ?instance WHERE {\n"
                + "   ?class rdfs:subClassOf* ?rootClass. \n"
                + "   ?instance rdf:type ?class .\n"
                + "}\n";

        ParameterizedSparqlString queryStr = new ParameterizedSparqlString(queryString);
        queryStr.setParam("rootClass", rootClass);
        Query query = QueryFactory.create(queryStr.toString());
        QueryExecution qexec = QueryExecutionFactory.create(query, dataSource.getModel());
        ResultSet resSet = qexec.execSelect();

        while (resSet.hasNext()) {
            QuerySolution querySolution = resSet.next();
            Resource res = querySolution.getResource("instance");

            if (res.getURI() != null) {
                Individual instance = dataSource.getModel().getIndividual(res.getURI());
                retInstances.add(instance);
            }
        }

        return retInstances;
    }

    /**
     * Convenient method returns a literal property's content if set, otherwise
     * the default value will be returned.
     *
     * @param individualURI
     * @param propertyURI
     * @param defaultValue
     * @return
     */
    public String getLiteralPropertyString(String individualURI, String propertyURI, String defaultValue) {
        if (propertyURI == null || "".equals(propertyURI.trim())) {
            return defaultValue;
        }
        Individual individual = dataSource.getModel().getIndividual(individualURI);
        if (individual == null) {
            return defaultValue;
        }
        Property property = dataSource.getModel().getProperty(propertyURI);
        if (property == null) {
            return defaultValue;
        }
        RDFNode value = individual.getPropertyValue(property);
        if (value == null) {
            return defaultValue;
        }
        return value.asLiteral().getString();
    }

    public static String getLabel(Resource resource) {
        String label = null;
        if (resource instanceof OntResource) {
            OntResource ontResource = (OntResource) resource;
            label = firstNonNull(ontResource.getLabel(CBR.LANGUAGE), ontResource.getLabel(CBR.ALTERNATIVE_LANGUAGE), ontResource.getLabel(null));
        }
        label = firstNonNull(label, resource.getLocalName(), "no label");
        return label;
    }

    public static String getLabel(Resource resource, String language) {
        String label = "Not defined";
        if (resource instanceof OntResource && ((OntResource) resource).getLabel(language) != null) {
            label = ((OntResource) resource).getLabel(language);
        } else {
            label = getLabel(resource);
        }
        return label;
    }

    public static String firstNonNull(String... paramters) {
        for (String parameter : paramters) {
            if (parameter != null) {
                return parameter;
            }
        }
        return null;
    }

    /**
     * Reload configuration properties first and then reload the models based on
     * the reloaded configuration settings.
     *
     * @param newConfiguration
     */
    public synchronized void refreshSingleton(Configuration newConfiguration) {
        CBR.CONFIG = newConfiguration;
        dataSource = new DefaultOntDataSource();
    }
    
    public void persistData(){
        dataSource.persistData();
    }


}
