/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.adaption;

import ch.fhnw.cbr.model.CBR_NS;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.shared.ReificationStyle;
import javax.inject.Singleton;
import org.topbraid.spin.inference.SPINInferences;
import org.topbraid.spin.system.SPINModuleRegistry;

/**
 * Provides methods to run adaption rules for inferencing of 
 * automatic adaption of case file items to the query case.
 * 
 * Prerequisite: Query case and historical case must be temporarly associated 
 *               via the property cbr:_automaticAdaptFromCase.
 * 
 * @author sandro.emmenegger
 */
@Singleton
public class AdaptionReasoning {
    
    public Model applyRules(String newCaseURI, String selectedCaseURI){
        OntModel model = OntAO.getInstance().getOntModel();
        Individual newCase = model.getIndividual(newCaseURI);
        Individual selectedCase = model.getIndividual(selectedCaseURI);
        OntProperty tempProperty = model.getOntProperty(CBR_NS.CBR+"_automaticAdaptFromCase");
        model.add(newCase, tempProperty, selectedCase);
        Model inferedItems = applyRules();
        inferedItems.write(System.out, "TTL");
        model.remove(newCase, tempProperty, selectedCase);
        return inferedItems;
    }
    
    public Model applyRulesAndAdd(String newCaseURI, String selectedCaseURI){
        Model inferedModel = applyRules(newCaseURI, selectedCaseURI);
        OntAO.getInstance().getOntModel().add(inferedModel);
        return inferedModel;
    }
    
    public Model applyRules(){
        // Initialize system functions and templates
		SPINModuleRegistry.get().init();

		// Load domain model with imports
		Model model = OntAO.getInstance().getOntModel();
		OntModel apprisModel =  ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, model);
		
		// Register any new functions defined in EO
                //SPINFunctions.init();
		SPINModuleRegistry.get().registerAll(apprisModel, null);

		// Create and add Model for inferred triples
		Model newTriples = ModelFactory.createDefaultModel(ReificationStyle.Minimal);
		apprisModel.addSubModel(newTriples);
		SPINInferences.run(apprisModel, newTriples, null, null, false, null);
		
		return newTriples;
    }
    
}
