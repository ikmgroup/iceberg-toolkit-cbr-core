/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.model.sim.PropertySimResult;
import ch.fhnw.cbr.model.sim.SimResult;
import ch.fhnw.cbr.model.sim.function.SimiliarityFunctionRegistry;
import ch.fhnw.cbr.model.sim.function.LocalSimilarityFunction;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.NodeIterator;

/**
 *
 * @author sandro.emmenegger
 */
public class CCLiteralProperty extends CCNode<OntProperty>{
    
    protected LocalSimilarityFunction simFunction;
    protected OntClass parentClass;
    
    private CCLiteralProperty(){};
    
    public CCLiteralProperty(OntClass parentClass, OntProperty ontProperty, String simFunctionName, float weight){
        assert ontProperty != null : "Resource for CCLiteralProperty must not be null.";
        assert SimiliarityFunctionRegistry.containsLocalFunction(simFunctionName) 
                : "Local similarity function not registered: "+simFunctionName;
        assert weight >= 0.0 : "Weight must be positive: " + weight;
        
        this.parentClass = parentClass;
        this.resource = ontProperty;
        this.simFunction = SimiliarityFunctionRegistry.getLocalFunction(simFunctionName);
        setSimilarityWeight(weight);
    }

    @Override
    public SimResult compare(Individual queryCaseInst, Individual caseInst) {
        
        PropertySimResult similarityLog = new PropertySimResult(resource.getURI(), simFunction.getClass().getSimpleName());
        
        Literal sourceValue = null;
        NodeIterator valuesItr = queryCaseInst.listPropertyValues(resource);
        if (valuesItr.hasNext()){
            sourceValue = valuesItr.next().asLiteral();
            similarityLog.setSourceValue(String.valueOf(sourceValue.getValue()));
            similarityLog.setSourceNull(false);
        }
        
        Literal targetValue = null;
        valuesItr = caseInst.listPropertyValues(resource);
        if (valuesItr.hasNext()){
            targetValue = valuesItr.next().asLiteral();
            similarityLog.setTargetValue(String.valueOf(targetValue.getValue()));
            similarityLog.setTargetNull(false);
        }
        
        if (!similarityLog.isSourceNull() && !similarityLog.isTargetNull()){
            simFunction.prepare(parentClass, resource);
            Double simValue = simFunction.compute(sourceValue.getValue(), targetValue.getValue());
            similarityLog.setSimilarityValue(simValue);
        }
        
        return similarityLog;
    }
    
    
    @Override
    public String printCaseCharactersiation(Individual instance, int level) {
        StringBuilder retVal = new StringBuilder();
        NodeIterator valuesItr = instance.listPropertyValues(resource);
        if (!valuesItr.hasNext()){
            Literal value = valuesItr.next().asLiteral();
            if (value != null){
                for (int i = 0; i < level; i++) {
                    retVal.append("    ");
                }
                retVal.append(getLabel(CBR.LANGUAGE)).append(" = ").append(value.toString()+"\n");
            }
        }       
        
        return retVal.toString();
    }    
}
