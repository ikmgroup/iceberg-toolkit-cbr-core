/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.model.sim.CaseMatch;
import ch.fhnw.cbr.model.sim.SimResult;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * A case view represents a part of the whole case charakterisation space. This
 * part of the characterisations addresses specific concerns of a business actor
 * resp. his business role.
 *
 * @author sandro.emmenegger
 */
public class CBRCaseView extends CBRBaseClass {

    public static final String CLASS_ID = CBR_NS.CBR + "CaseView";

    private static final Logger LOG = Logger.getLogger(CBRCaseView.class.getName());

    private List<CBRConcern> addressedConcerns;

    public CBRCaseView(String uri) {
        super(uri);
    }

    private CBRCaseView(Individual individual) {
        super(individual);
    }

    public static List<CBRCaseView> getInstances() {
        OntClass ontClass = OntAO.getInstance().getOntModel().getOntClass(CLASS_ID);
        List<Individual> instances = OntAO.getInstance().getInstances(ontClass);
        List<CBRCaseView> caseViews = new ArrayList<>();
        for (Individual individual : instances) {
            caseViews.add(new CBRCaseView(individual));
        }
        return caseViews;
    }

    public static CBRCaseView findCaseViewByLocalName(String localName) {
        List<CBRCaseView> instances = getInstances();
        for (CBRCaseView caseView : instances) {
            if (caseView.getIndividual().getLocalName().equals(localName)) {
                return caseView;
            }
        }
        return null;
    }

    /**
     * Returns ontology representation of the context class.
     *
     * @return
     */
    public static OntClass ontClass() {
        return OntAO.getInstance().getOntModel().getOntClass(CLASS_ID);
    }

    public static CBRCaseView findCaseViewByURI(String caseViewURI) {
        List<CBRCaseView> instances = getInstances();
        for (CBRCaseView caseView : instances) {
            if (caseView.getIndividual().getURI().equals(caseViewURI)) {
                return caseView;
            }
        }
        return null;
    }

    /**
     * Compares query case with all cases in the case base.
     *
     * @param queryCaseInst
     * @return
     */
    public List<CaseMatch> retreive(Individual queryCaseInst) {

        CBRCase ccCase = new CBRCase(this);
        List<CaseMatch> retCases = new ArrayList<>();
        //iterate over case instances and call compare
        List<Individual> caseInstances = ccCase.getInstances();
        for (Individual caseInst : caseInstances) {
            LOG.fine("\n------ Case: " + caseInst.getLocalName() + " (" + caseInst.getURI() + ")--------------");
            if(CBRCaseState.LEARNED_CASE_STATE == CBRCase.getCaseState(caseInst)){
                SimResult simResult = ccCase.compare(queryCaseInst, caseInst);
                LOG.fine(simResult.toString());
                LOG.fine("--------------------------------------------------------------------------------\n");
                if (simResult != null) {
                    retCases.add(new CaseMatch(caseInst, simResult));
                }
            }
        }

        return retCases;
    }

    public List<CBRConcern> getAddressedConcerns() {
        if (addressedConcerns == null) {
            addressedConcerns = new ArrayList<>();

            String queryString
                    = "PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                    + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                    + "PREFIX cbr:  <http://ikm-group.ch/cbr#>\n"
                    + "SELECT ?concern WHERE {\n"
                    + "   ?view cbr:caseViewAddressesConcerns ?concern. \n"
                    + "}\n";

            ParameterizedSparqlString queryStr = new ParameterizedSparqlString(queryString);
            queryStr.setParam("view", getIndividual());
            Query query = QueryFactory.create(queryStr.toString());
            QueryExecution qexec = QueryExecutionFactory.create(query, OntAO.getInstance().getOntModel());
            ResultSet resSet = qexec.execSelect();

            while (resSet.hasNext()) {
                QuerySolution querySolution = resSet.next();
                Resource res = querySolution.getResource("concern");
                CBRConcern concern = new CBRConcern(res.getURI());
                addressedConcerns.add(concern);
            }
        }
        return addressedConcerns;
    }

    public void setAddressedConcerns(List<CBRConcern> addressedConcerns) {
        this.addressedConcerns = addressedConcerns;
    }

}
