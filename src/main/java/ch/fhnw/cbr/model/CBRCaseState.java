/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author sandro.emmenegger
 */
public enum CBRCaseState {

    ADAPTATION_CASE_STATE(CBR_NS.CBR + "Adaptation_CaseState"),
    RREVISION_CASE_STATE(CBR_NS.CBR + "Revision_CaseState"),
    LEARNED_CASE_STATE(CBR_NS.CBR + "Learned_CaseState");

    private String uri;
    private final Individual individual;

    private CBRCaseState(String uri) {
        this.uri = uri;
        individual = OntAO.getInstance().getOntModel().getIndividual(uri);
    }

    public Individual getIndividual() {
        return individual;
    }
    
    public static CBRCaseState get(String uri){
        for(CBRCaseState state : getAllCaseStates()){
            if(state.uri.equals(uri)){
                return state;
            }
        }
        return null;
    }

    /**
     * Searchs for all instances of the class cbr:CaseState and it's subclasses.
     *
     * @return List of BusinessRoles
     */
    static public List<CBRCaseState> getAllCaseStates() {
        return Arrays.asList(CBRCaseState.values());
    }

    public String getLabel(String languageCode) {
        String label = "";
        if (individual != null) {
            label = individual.getLabel(languageCode.toUpperCase());
            if (label == null) {
                label = individual.getURI();
            }
        }
        return label.trim();
    }

    public String getLabel() {
        return getLabel(CBR.LANGUAGE);
    }

}
