/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Literal;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author sandro.emmenegger
 */
public class CBRCaseStateHist extends CBRBaseClass{
    
    public static final String CLASS_ID = CBR_NS.CBR + "CaseStateHistory";
    
    private static final String TIMESTAMP_DATA_PROPERTY = CBR_NS.CBR + "caseStateTimestamp";
    private static final String CASE_OBJECT_PROPERTY = CBR_NS.CBR + "belongsToCase";
    private static final String CASE_STATE_OBJECT_PROPERTY = CBR_NS.CBR + "hasCaseState";

    private CBRCaseStateHist(String uri) {
        this(OntAO.getInstance().getOntModel().getOntClass(CLASS_ID).createIndividual(uri));
    }
    
    public CBRCaseStateHist(Individual caseStateHistIndividual) {
        super(caseStateHistIndividual);
    }
    
    public CBRCaseStateHist(String uri, CBRCaseState caseState, Individual cbrCase) {
        this(uri);
        OntModel model = OntAO.getInstance().getOntModel();
        
        
        //add timestamp
       	Calendar cal = GregorianCalendar.getInstance();
	Literal value = model.createTypedLiteral(cal);
        getIndividual().addProperty(model.getOntProperty(TIMESTAMP_DATA_PROPERTY), value);
        
        //add case state
        getIndividual().addProperty(model.getOntProperty(CASE_STATE_OBJECT_PROPERTY), caseState.getIndividual());
        //add case instance
        getIndividual().addProperty(model.getOntProperty(CASE_OBJECT_PROPERTY), cbrCase);
    }
    
}
