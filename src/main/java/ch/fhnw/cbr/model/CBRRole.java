/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


/**
 * CBR Role class encapsulates reference to Jena Individual representing an
 ontology instance of the class cbr:Role or a subclass of it.
 *
 * @author sandro.emmenegger
 */
public class CBRRole extends CBRBaseClass {

    public static final String CLASS_ID = CBR_NS.CBR + "Role";

    private static final Logger LOG = Logger.getLogger(CBRRole.class.getName());

    public CBRRole(String uri) {
        super(uri);
    }

    public CBRRole(Individual individual) {
        super(individual);
    }

    /**
     * Searchs for all instances of the class archi:CBRRole and it's subclasses.
     *
     * @return List of BusinessRoles
     */
    static public List<CBRRole> getAllRoles() {
        OntClass ontClass = OntAO.getInstance().getOntModel().getOntClass(CLASS_ID);
        List<Individual> instances = OntAO.getInstance().getInstances(ontClass);
        List<CBRRole> roles = new ArrayList<>();
        for (Individual instance : instances) {
            roles.add(new CBRRole(instance));
        }
        return roles;
    }
}
