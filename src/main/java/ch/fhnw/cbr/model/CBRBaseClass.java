/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sandro.emmenegger
 */
public abstract class CBRBaseClass {

    private final Individual individual;
    
    private static final Logger LOG = Logger.getLogger(CBRBaseClass.class.getName());

    public CBRBaseClass(String uri) {
        individual = OntAO.getInstance().getOntModel().getIndividual(uri);
        if (individual == null){
            LOG.log(Level.SEVERE, "Cannot initialize instance of class due to wrong instancce URI: {0}", uri);
        }
    }

    public CBRBaseClass(Individual individual) {
        this.individual = individual;
    }

    public Individual getIndividual() {
        return individual;
    }

    public String getLocalName() {
        return individual.getLocalName();
    }

    public String getLabel() {
        return getLabel(CBR.LANGUAGE);
    }
    
    public String getLabel(String languageCode) {
        String label = "";
        if (individual != null) {
            label = individual.getLabel(languageCode.toUpperCase());
            if (label == null) {
                label = individual.getURI();
            }
        }
        return label.trim();
    }    

}
