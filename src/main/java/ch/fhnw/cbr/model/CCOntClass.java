/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.core.queries.QueryMap;
import ch.fhnw.cbr.model.sim.ObjectSimResult;
import ch.fhnw.cbr.model.sim.SimResult;
import ch.fhnw.cbr.model.sim.WeightedDouble;
import ch.fhnw.cbr.model.sim.function.GlobalSimilarityFunction;
import ch.fhnw.cbr.model.sim.function.SimiliarityFunctionRegistry;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.AnnotationProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a CBR characterization resource.
 *
 * @author sandro.emmenegger
 */
public class CCOntClass extends CCNode<OntClass> {

    protected CBRCaseView caseView;
    protected GlobalSimilarityFunction simFunction;
    
    private final List<CCNode> properties = new ArrayList<>();

    protected CCOntClass() {
    }
    
    public CCOntClass(CBRCaseView caseView, OntClass ontClass, GlobalSimilarityFunction simFunctionName) {
        assert caseView != null : "View on case for CCResource must not be null.";
        assert ontClass != null : "Resource for CCResource must not be null.";
        assert simFunctionName != null;

        this.caseView = caseView;
        this.resource = ontClass;
        this.simFunction = simFunctionName;
        
        init();
    }
    
    protected void init(){
        loadCCLiteralProperties();
        loadCCObjectProperties();
    }

    /**
     * Returns all instances of the OntClass represented by this java class
     * including instances of subclasses.
     *
     * @return
     */
    public List<Individual> getInstances() {
        return OntAO.getInstance().getInstances(resource);
    }
    
    /**
     * Get properties.
     *
     * @return
     */
    public List<CCNode> getProperties() {
        return properties;
    }


    private void loadCCLiteralProperties() {
        String queryString = QueryMap.getQuery("getLiteralProperties");
        ParameterizedSparqlString queryStr = new ParameterizedSparqlString(queryString);
        queryStr.setParam("caseView", caseView.getIndividual());
        queryStr.setParam("class", resource);
        Query query = QueryFactory.create(queryStr.toString());
        QueryExecution qexec = QueryExecutionFactory.create(query, OntAO.getInstance().getOntModel());
        ResultSet resSet = qexec.execSelect();

        while (resSet.hasNext()) {
            QuerySolution querySolution = resSet.next();
            float similarityWeight = querySolution.getLiteral("weight").getFloat();
            Resource simFunctionRes = querySolution.getResource("simFunction");

            String simFunctionName = simFunctionRes.getLocalName();

            Resource res = querySolution.getResource("literalProperty");
            CCLiteralProperty ccProperty = null;
            
            //Annnotation property
            if(CBR.RDFS_LABEL.equals(res.getURI()) || CBR.RDFS_COMMENT.equals(res.getURI())){
                    AnnotationProperty annotationProp = res.as(AnnotationProperty.class);
                    ccProperty = new CCAnnotationProperty(resource, annotationProp, simFunctionName, similarityWeight);
            }
            else{ //Datatype property
                OntProperty property = res.as(OntProperty.class);
                ccProperty = new CCLiteralProperty(resource, property, simFunctionName, similarityWeight);
            }
            properties.add(ccProperty);
        }

        qexec.close();
    }

    private void loadCCObjectProperties() {

        String queryString = QueryMap.getQuery("getObjectProperties");
        ParameterizedSparqlString queryStr = new ParameterizedSparqlString(queryString);
        queryStr.setParam("caseView", caseView.getIndividual());
        queryStr.setParam("class", resource);
        Query query = QueryFactory.create(queryStr.toString());
        QueryExecution qexec = QueryExecutionFactory.create(query, OntAO.getInstance().getOntModel());
        ResultSet resSet = qexec.execSelect();

        while (resSet.hasNext()) {
            QuerySolution querySolution = resSet.next();
            Resource res = querySolution.getResource("objectProperty");
            OntProperty ontProp = res.as(OntProperty.class);

            float similarityWeight = querySolution.getLiteral("weight").getFloat();
            Resource refAggregationFunctionRes = querySolution.getResource("aggregationFunction");
            String refAggregationFunctionName = refAggregationFunctionRes.getLocalName();
            
            boolean allowNewInstances = true;
            
            CCRefProperty refProperty = null;
            Resource refTaxonomySimFunctionRes = querySolution.getResource("taxonomySimFunction");
            if(refTaxonomySimFunctionRes != null){
                allowNewInstances = false;
                String refTaxonomySimFunction = refTaxonomySimFunctionRes.getLocalName();
                String refTaxonomySimilaritySetId = querySolution.getLiteral("taxonomySimilaritySetId").getString();
                
                //if taxonomy based on recursive property
                OntProperty recursiveObjectProperty = null;
                boolean recursivePropertyExpressesChildOf = false;
                
                Resource recursiveObjectPropertyRes = querySolution.getResource("recursiveObjectProperty");
                if(recursiveObjectPropertyRes != null){
                    recursiveObjectProperty = recursiveObjectPropertyRes.as(OntProperty.class);
                    String recursivePropertyDirection = querySolution.getLiteral("recursivePropertyDirection").getString();
                    recursivePropertyExpressesChildOf = "expressesChildOf".equals(recursivePropertyDirection);
                }
                refProperty = new CCRefProperty(caseView, ontProp, refAggregationFunctionName, similarityWeight, allowNewInstances, refTaxonomySimFunction, refTaxonomySimilaritySetId, recursiveObjectProperty, recursivePropertyExpressesChildOf);
            }else{
                refProperty = new CCRefProperty(caseView, ontProp, refAggregationFunctionName, similarityWeight, allowNewInstances);
            }            
            
            properties.add(refProperty);
        }
        qexec.close();
    }

    @Override
    public SimResult compare(Individual queryCaseInst, Individual caseInst) {

        assert queryCaseInst != null && caseInst != null : "Expect individuals to be compared not to be null";

        String simFunctionName = simFunction.getClass().getSimpleName();
        ObjectSimResult similarityLog = new ObjectSimResult(resource.getURI(), simFunctionName);
        List<WeightedDouble> allPropertiesSimValues = new ArrayList<>();

        //get similarity of all properties where query property is set
        for (CCNode cCProperty : properties) {

            SimResult simResult = cCProperty.compare(queryCaseInst, caseInst);
            if (!simResult.isSourceNull()) {
                similarityLog.setSourceNull(false);
            }
            if (!simResult.isTargetNull()) {
                similarityLog.setTargetNull(false);
            }

            if (!simResult.isSourceNull() && !simResult.isTargetNull()) {
                allPropertiesSimValues.add(new WeightedDouble(simResult.getSimilarityValue(), cCProperty.getSimilarityWeight()));
            }
            similarityLog.getSubResults().add(simResult);
        }

        similarityLog.setSimilarityValue(simFunction.computeLocalSimilarities(allPropertiesSimValues, true));

        return similarityLog;
    }

    @Override
    public String printCaseCharactersiation(Individual instance, int level) {

        StringBuilder temp = new StringBuilder();
        for (CCNode cCProperty : properties) {
            if (instance.getProperty(OntAO.getInstance().getOntModel().getProperty(cCProperty.getURI())) != null) {
                temp.append(cCProperty.printCaseCharactersiation(instance, level++));
            }
        }

        StringBuilder retVal = new StringBuilder();
        if (temp.length() > 0) {
            for (int i = 0; i < level; i++) {
                retVal.append("    ");
            }
            retVal.append(getLabel(CBR.LANGUAGE)).append("\n");
            retVal.append(temp);
        }

        return retVal.toString();
    }

    public GlobalSimilarityFunction getGlobalSimilarityFunction() {
        return simFunction;
    }
}
