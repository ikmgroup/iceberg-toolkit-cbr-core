/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.model.sim.SimResult;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 *
 * @author sandro.emmenegger
 */
abstract public class CCNode<T extends Resource> {
    
    protected T resource;
    
    private float similarityWeight;
    
    abstract public SimResult compare(Individual source, Individual target);
    
    abstract String printCaseCharactersiation(Individual instance, int level);

    public String getURI(){
        return resource.getURI();
    }
    
    public String getLocalName(){
        return resource.getLocalName();
    }
    
    public String getLabel() {
        return OntAO.getLabel(resource);
    }    
    
    public String getLabel(String language){
        return OntAO.getLabel(resource, language);
    }
    
    /**
     * @return the similarityWeight
     */
    public float getSimilarityWeight() {
        return similarityWeight;
    }
    
    /**
     * @param similarityWeight the similarityWeight to set
     */
    protected void setSimilarityWeight(float similarityWeight) {
        this.similarityWeight = similarityWeight;
    }
    
    protected String firstNonNull(String... paramters) {
        for (String parameter : paramters) {
            if (parameter != null) {
                return parameter;
            }
        }
        return null;
    }    
    
    @Override
    public String toString(){
        if(resource == null) return super.toString();
        
        return resource.getLocalName();
    }

}
