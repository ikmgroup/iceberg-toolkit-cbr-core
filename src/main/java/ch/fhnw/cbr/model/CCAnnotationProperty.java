/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.model.sim.PropertySimResult;
import ch.fhnw.cbr.model.sim.SimResult;
import com.hp.hpl.jena.ontology.AnnotationProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;

/**
 *
 * @author sandro.emmenegger
 */
public class CCAnnotationProperty extends CCLiteralProperty {

    public CCAnnotationProperty(OntClass parentClass, AnnotationProperty ontProperty, String simFunctionName, float weight) {
        super(parentClass, ontProperty, simFunctionName, weight);
    }

    @Override
    public SimResult compare(Individual queryCaseInst, Individual caseInst) {
        
        PropertySimResult similarityLog = new PropertySimResult(resource.getURI(), simFunction.getClass().getSimpleName());
        
        String queryValue = getValue(queryCaseInst);
        if (queryValue != null){
            similarityLog.setSourceNull(false);
            similarityLog.setSourceValue(queryValue);
        }
        
        String targetValue = getValue(caseInst);             
        if (targetValue != null){
            similarityLog.setTargetNull(false);
            similarityLog.setTargetValue(targetValue);
        }

         if (!similarityLog.isSourceNull() && !similarityLog.isTargetNull()){
             simFunction.prepare(parentClass, resource);
             Double simValue = simFunction.compute(queryValue, targetValue);
             similarityLog.setSimilarityValue(simValue);
         }

        return similarityLog;
    }

    public String getValue(Individual instance) {
        if ("Label".equalsIgnoreCase(resource.getLocalName())) {
            String retVal = firstNonNull(instance.getLabel(CBR.LANGUAGE), instance.getLabel(CBR.ALTERNATIVE_LANGUAGE), instance.getLabel(null));
            return retVal;
        }
        if ("Comment".equalsIgnoreCase(resource.getLocalName())) {
            String retVal = firstNonNull(instance.getComment(CBR.LANGUAGE), instance.getComment(CBR.ALTERNATIVE_LANGUAGE), instance.getComment(null));
            return retVal;
        }
        return null;
    }
    
    @Override
    public String printCaseCharactersiation(Individual instance, int level) {
        StringBuilder retVal = new StringBuilder();
        String value = getValue(instance);
        if (value != null){
            for (int i = 0; i < level; i++) {
                retVal.append("    ");
            }
            retVal.append(getLabel(CBR.LANGUAGE)).append(" = ").append(value.toString()).append("\n");
        }
        
        return retVal.toString();
    }       
}
