/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.local;

import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.model.sim.function.AbstractLocalSimilarityFunction;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.wcohen.ss.BasicStringWrapper;
import com.wcohen.ss.BasicStringWrapperIterator;
import com.wcohen.ss.SoftTFIDF;
import com.wcohen.ss.api.StringWrapper;
import com.wcohen.ss.api.StringWrapperIterator;
import com.wcohen.ss.tokens.SimpleTokenizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Compares strings according SoftTFIDF and Jaro-Winkler. 
 * 
 * Uses the scondstring library <see>http://secondstring.sourceforge.net/javadoc/com/wcohen/ss/SoftTFIDF.html</see>
 * and <see>http://secondstring.sourceforge.net/javadoc/com/wcohen/ss/JaroWinkler.html</see>
 * 
 * @author sandro.emmenegger
 */
public class SoftTFIDFJaroWinkler extends AbstractLocalSimilarityFunction{
    
    /** The minimum edit-distance-based similarity that tokens should have in order to
	be considered as identical (if set to 1, we only use exact match) */
    private static final double MIN_SIM = 0.95;
    
    private com.wcohen.ss.SoftTFIDF tfidf;

    public SoftTFIDFJaroWinkler(){
        tfidf = new SoftTFIDF(new SimpleTokenizer(true, true), new com.wcohen.ss.JaroWinkler(), MIN_SIM);
    }
    
    @Override
    public void prepare(OntClass ontClass, OntProperty ontProperty) {
        ExtendedIterator<Individual> itr = OntAO.getInstance().getOntModel().listIndividuals(ontClass);
        Collection<StringWrapper> values = new ArrayList<>();
        while (itr.hasNext()) {
            Individual individual = itr.next();
            Iterator<RDFNode> valuesItr = null;
            if (ontProperty.isAnnotationProperty()){
                if ("Label".equalsIgnoreCase(ontProperty.getLocalName())) {
                     valuesItr =individual.listLabels(CBR.LANGUAGE);
                }
                if ("Comment".equalsIgnoreCase(ontProperty.getLocalName())) {
                    valuesItr =individual.listComments(CBR.LANGUAGE);
                }
            }else{
                valuesItr = individual.listPropertyValues(ontProperty);
            }
            
            while (valuesItr != null && valuesItr.hasNext()) {
                RDFNode rDFNode = valuesItr.next();
                values.add(new BasicStringWrapper(rDFNode.asLiteral().getValue().toString()));
            }
        }
        StringWrapperIterator valuesItrator = new BasicStringWrapperIterator(values.iterator());
        tfidf.train(valuesItrator);
    }

    @Override
    public double compute(Object source, Object target) {
        
        if(source == target) return 1.0;
        if(source == null || target == null) return 0.0;
 
        return tfidf.score(source.toString(), target.toString());
    }
    
}
