/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;

/**
 * Subclasses must implement a function to calculate the similarity of two values.
 * @author sandro.emmenegger
 */
public interface LocalSimilarityFunction {
 
    /**
     * Calculate the similarity of two given values.
     * 
     * @param source
     * @param target
     * @return similarity measure between inclusive 0 and 1.
     */
    public double compute(Object source, Object target );
    
    /**
     * Allows to prepare a similarity function.
     * @param ontClass 
     * @param ontProperty
     */
    public void prepare(OntClass ontClass, OntProperty ontProperty);
}
