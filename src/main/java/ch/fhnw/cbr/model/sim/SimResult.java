/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim;

/**
 * Base class representing a global or local comparsion result.
 * 
 * @author sandro.emmenegger
 */
public class SimResult {
   
    private String uri;
    private String similarityFunctionName;
    private Double similarityValue;
    private boolean sourceNull = true;
    private boolean targetNull = true;
    private String comment = "";
    
    public SimResult(String uri, String similarityFunctionName) {
        this.uri = uri;
        this.similarityFunctionName = similarityFunctionName;
    }
    
    public String getUriShort(){
        String shortUri = null;
        if (uri != null && uri.contains("/")){
            shortUri = uri.substring(uri.lastIndexOf("/")+1);
            shortUri = shortUri.replace("#", ":");
        }
        return shortUri;
    }
    
    public SimResult copy(){
        SimResult copy = new SimResult(uri, similarityFunctionName);
        copy.setSimilarityValue(similarityValue);
        copy.setSourceNull(sourceNull);
        copy.setTargetNull(targetNull);
        copy.setComment(comment);
        
        return copy;
    }
    
    public String getURI() {
        return uri;
    }
    
    public void setURI(String uri) {
        this.uri = uri;
    }

    public String getSimilarityFunctionName() {
        return similarityFunctionName;
    }

    public void setSimilarityFunctionName(String similarityFunctionName) {
        this.similarityFunctionName = similarityFunctionName;
    }

    public Double getSimilarityValue() {
        return similarityValue;
    }

    public void setSimilarityValue(Double similarityValue) {
        this.similarityValue = similarityValue;
    }

    public boolean isSourceNull() {
        return sourceNull;
    }

    public void setSourceNull(boolean sourceNull) {
        this.sourceNull = sourceNull;
    }

    public boolean isTargetNull() {
        return targetNull;
    }

    public void setTargetNull(boolean targetNull) {
        this.targetNull = targetNull;
    }
    
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }    
    
    @Override
    public String toString() {
        return getUriShort()+"("+similarityFunctionName+", "+similarityValue+") "+comment;
    }    
}
