/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.global;

import ch.fhnw.cbr.model.sim.ObjectSimResult;
import ch.fhnw.cbr.model.sim.SimResult;
import ch.fhnw.cbr.model.sim.WeightedDouble;
import ch.fhnw.cbr.model.sim.function.GlobalSimilarityFunction;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sandro.emmenegger
 */
public class Average implements GlobalSimilarityFunction
{
    @Override
    public double computeLocalSimilarities(List<WeightedDouble> values, boolean normalize){
        
        assert values != null : "Null value for value aggregation not allowed.";

        if (values.isEmpty()) {
            return 0.0;
        }
        double retVal = 0.0;
        double sumWeight = 0.0;
        
        for (WeightedDouble value : values) {
            retVal += value.getValue()*value.getWeight();
            if(normalize){
                sumWeight += value.getWeight();
            }
        }
        
        if (normalize){
            retVal = retVal/sumWeight;
        }
        
        return retVal;
    }

    
    /**
     * Calculates the average similarity over the similarites of all query target
     * instances comparsions.
     * 
     * Ex. comparing 2 query instances {q1, q2} with 3 targetInstances {t1, t2, t3}:
     * 
     * Expexted matrix passed to this method:
     * 
     *          query vectors                           target vectors
     * _____________________________    ___________________________________________
     *|                             |  |                                           |
     *      q1              q2              t1            t2              t3
     * q1 | sim(q1,q1)   sim(q2,q1)     sim(t1,q1)      sim(t2,q1)     sim(t3,q1)   
     * q2 | sim(q1,q2)   sim(q2,q2)     sim(t1,q2)      sim(t2,q2)     sim(t3,q2)
     * t1 | sim(q1,t1)   sim(q2,t1)     sim(t1,t1)      sim(t2,t1)     sim(t3,t1)
     * t2 | sim(q1,t2)   sim(q2,t2)     sim(t1,t2)      sim(t2,t2)     sim(t3,t2)
     * t3 | sim(q1,t3)   sim(q2,t3)     sim(t1,t3)      sim(t2,t3)     sim(t3,t3)
     * 
     * SIMILARITY = w*sim(q1,t1) + w*sim(q1,t2) + w* sim(q1,t3) + w*sim(q1,t1) + w*sim(q1,t2) + w*sim(q1,t3)
     * 
     * where w is the average weight, means in our case = 1/size = 1/5 = 0.2
     * 
     * @param simMatrix
     * @param querySize
     * @return 
     */
    @Override
    public ObjectSimResult computeObjectPropertySimilarity(SimResult[][] simMatrix, int querySize) {
        assert simMatrix != null : "Null value for value aggregation not allowed.";
        assert simMatrix.length > 0 : "Expect values to aggregate but list is empty.";
        
        ObjectSimResult simResult = new ObjectSimResult(null, null);
        List<WeightedDouble> values = new ArrayList<>();
        for(int col=0; col < querySize; col++){
            for(int row=querySize; row<simMatrix.length; row++){
                values.add(new WeightedDouble(simMatrix[col][row].getSimilarityValue(), 1.0));
                simResult.getSubResults().add(simMatrix[col][row]);
            }
        }
        simResult.setSimilarityValue(computeLocalSimilarities(values, true));
        return simResult;
    }
    
}
