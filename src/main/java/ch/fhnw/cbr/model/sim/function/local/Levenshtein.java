/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.local;

import ch.fhnw.cbr.model.sim.function.AbstractLocalSimilarityFunction;

/**
 * Uses the scondstring library <see>http://secondstring.sourceforge.net/javadoc/com/wcohen/ss/Level2Levenstein.html</see>
 * @author sandro.emmenegger
 */
public class Levenshtein extends AbstractLocalSimilarityFunction{

    @Override
    public double compute(Object source, Object target) {
        
        if(source == target) return 1.0;
        if(source == null || target == null) return 0.0;
        
        com.wcohen.ss.Level2Levenstein function = new com.wcohen.ss.Level2Levenstein();
        double score = function.score(source.toString(), target.toString());
        double simValue = score == 0.0 ? 1.0 : Math.abs(1/(score-1)); 

        return simValue;
    }
    
}
