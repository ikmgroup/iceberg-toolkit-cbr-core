/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.local.taxonomy;

import ch.fhnw.cbr.model.sim.function.TaxonomySimilarityFunction;
import ch.fhnw.cbr.model.sim.function.local.taxonomy.utils.OntTools;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import java.util.Map;

/**
 * Similarity function based on taxonomic postion of common predecessor.
 * Considers either a ineheritance based structure (rdfs:subClassOf) 
 * or any recursive object property based parent/child structure.
 * 
 * @author sandro.emmenegger
 */
public class TaxonomyAnnotatedValue implements TaxonomySimilarityFunction {

    @Override
    public double compute(Individual sourceInstance, Individual targetInstance, Map<String, Double> taxonomyResourceValues, OntProperty recursiveObjectProperty, boolean expressesChildOf) {
        
        if(recursiveObjectProperty == null){
            return computeInheritanceStructure(sourceInstance, targetInstance, taxonomyResourceValues);
        }else{
            return computeRecursivePropertyStructure(sourceInstance, targetInstance, taxonomyResourceValues, recursiveObjectProperty, expressesChildOf);
        }
    }
    
    /**
     * Searchs for the lowest common ancestor (LCA) in the class hierarchy of the query (source) and target instances classes. 
     * Returns the annotated similarity value of the LCA. If no LCA is found, 0.0 is returned;
     * 
     * @param sourceInstance
     * @param targetInstance
     * @param taxonomyResourceValues
     * @return 
     */
    public double computeInheritanceStructure(Individual sourceInstance, Individual targetInstance, Map<String, Double> taxonomyResourceValues) {
        
        if(sourceInstance.getURI().equals(targetInstance.getURI())){
            return 1.0;
        }
        
        //find nearest common ancestor
        OntClass commonAncestor = OntTools.getLCA(sourceInstance.getOntClass(), targetInstance.getOntClass());
        if(commonAncestor != null && taxonomyResourceValues.containsKey(commonAncestor.getURI())){
            return taxonomyResourceValues.get(commonAncestor.getURI());
        }
        
        return 0.0;
    }    
    
    /**
     * Searchs for the lowest common ancestor (LCA) in the instance hierarchy of the query (source) and target instances.
     * The parent/child resp. child/parent property is considered to naviagte through the instances hierarchy. 
     * 
     * Returns the annotated similarity value of the LCA instance or if no value was assigned directly to the instance, the value of the class is considered.
     * If no value was annotated to both, the instance and the class, 0.0 is returned;
     * 
     * @param sourceInstance
     * @param targetInstance
     * @param taxonomyResourceValues
     * @param recursiveObjectProperty
     * @param expressesChildOf
     * @return 
     */
    public double computeRecursivePropertyStructure(Individual sourceInstance, Individual targetInstance, Map<String, Double> taxonomyResourceValues, OntProperty recursiveObjectProperty, boolean expressesChildOf) {
        
        //find nearest common ancestor
        Individual commonAncestor = OntTools.getLCA(sourceInstance, targetInstance, recursiveObjectProperty, expressesChildOf);
        if(commonAncestor == null){
            return 0.0;
        }
        if(taxonomyResourceValues.containsKey(commonAncestor.getURI())){
            return taxonomyResourceValues.get(commonAncestor.getURI());
        }
        if(taxonomyResourceValues.containsKey(commonAncestor.getOntClass().getURI())){
            return taxonomyResourceValues.get(commonAncestor.getOntClass().getURI());
        }
        
        return 0.0;
    }
    
}
