/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim;

/**
 * Encapsulates similarity results of two compared data properties.
 * 
 * @author sandro.emmenegger
 */
public class PropertySimResult extends SimResult{

    private String sourceValue;
    private String targetValue;
    
    public PropertySimResult(String uri, String similarityFunctionName) {
        super(uri, similarityFunctionName);
    }    
    
    @Override
    public PropertySimResult copy(){
        PropertySimResult copy = new PropertySimResult(getURI(), getSimilarityFunctionName());
        copy.setSimilarityValue(getSimilarityValue());
        copy.setSourceNull(isSourceNull());
        copy.setTargetNull(isTargetNull());
        copy.setComment(getComment());
        
        copy.setSourceValue(sourceValue);
        copy.setTargetValue(targetValue);
        return copy;
    }

    public String getSourceValue() {
        return sourceValue;
    }

    public void setSourceValue(String sourceValue) {
        this.sourceValue = sourceValue;
    }

    public String getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(String targetValue) {
        this.targetValue = targetValue;
    }

    @Override
    public String toString() {
        return getUriShort()+"('" + sourceValue + "', '" + targetValue + "', "+getSimilarityFunctionName()+", "+getSimilarityValue()+")";
    }
}
