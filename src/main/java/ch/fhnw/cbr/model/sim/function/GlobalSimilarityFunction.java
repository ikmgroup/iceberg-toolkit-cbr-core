/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function;

import ch.fhnw.cbr.model.sim.ObjectSimResult;
import ch.fhnw.cbr.model.sim.SimResult;
import ch.fhnw.cbr.model.sim.WeightedDouble;
import java.util.List;

/**
 * Subclasses must implement an aggregation function to calculate the 
 * similarity of a set of similarity values and their weights.
 * @author sandro.emmenegger
 */
public interface GlobalSimilarityFunction {
 
    /**
     * Calcualtate a weigthed similarity measure from  a list of weighted 
     * local similarites (similarity values of attributes).
     * 
     * @param values similarity values
     * @param normalize if set true, the weights will be normalized.
     * @return similarity measure between inclusive 0 and 1.
     */
    public double computeLocalSimilarities(List<WeightedDouble> values, boolean normalize);
    
    /**
     * Calculates a weighted similarity measure from a matrix of similarities based 
     * on the comparsion of all query and target instances of a relation (object property). 
     * 
     * @param simMatrix matrix of all similarity measures of compared query and target instances 
     * @param querySize the amount of query vectors in the matrix
     * @return 
     */
    public ObjectSimResult computeObjectPropertySimilarity(SimResult[][] simMatrix, int querySize);
}
