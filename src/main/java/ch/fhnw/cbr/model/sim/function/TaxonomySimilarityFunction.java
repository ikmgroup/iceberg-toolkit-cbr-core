/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntProperty;
import java.util.Map;

/**
 * Base Interface for all similarity algorithms considering the position of 
 * class instances (and ordered properties) as a measure for their similarity.
 *
 * @author sandro.emmenegger
 */
public interface TaxonomySimilarityFunction {

    /**
     * Similarity function must implement this method to compare class instances
     * (individuals).
     * 
     * @param sourceInstance
     * @param targetInstance
     * @param taxonomyResourceValues
     * @param recursiveObjectProperty
     * @param expressesChildOf
     * @return 
     */
    public double compute(Individual sourceInstance, Individual targetInstance, Map<String, Double> taxonomyResourceValues, OntProperty recursiveObjectProperty, boolean expressesChildOf);
}
