/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Base class for object property similarity calculation results.
 * Mainly encapsulates a list of sub property results (Composite Pattern).
 * 
 * @author sandro.emmenegger
 */
public class ObjectSimResult extends SimResult{
    
    private List<SimResult> subResults = new ArrayList<>();
    
    public ObjectSimResult(String uri, String similarityFunctionName) {
        super(uri, similarityFunctionName);
    }    
    
    @Override
    public ObjectSimResult copy(){
        ObjectSimResult copy = new ObjectSimResult(getURI(), getSimilarityFunctionName());
        copy.setSimilarityValue(getSimilarityValue());
        copy.setSourceNull(isSourceNull());
        copy.setTargetNull(isTargetNull());
        copy.setComment(getComment());
        List<SimResult> subResultsCopy = new ArrayList<>();
        for (SimResult subResult : subResults) {
            subResultsCopy.add(subResult.copy());
        }
        copy.setSubResults(subResultsCopy);
        return copy;
    }

    public List<SimResult> getSubResults() {
        return subResults;
    }

    public void setSubResults(List<SimResult> subResults) {
        this.subResults = subResults;
    }

    @Override
    public String toString() {
        
        StringBuilder retVal = new StringBuilder(super.toString());
        for (SimResult simResult : subResults) {
                StringBuilder subResultStr = new StringBuilder();
                Scanner scanner = new Scanner(simResult.toString());
                while(scanner.hasNextLine()) subResultStr.append("\n    ").append(scanner.nextLine());
                retVal.append(subResultStr);    
        }
        return retVal.toString();
    }
}
