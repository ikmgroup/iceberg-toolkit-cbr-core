/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim;

import ch.fhnw.cbr.model.CBRCase;
import ch.fhnw.cbr.model.CBRCaseState;
import com.hp.hpl.jena.ontology.Individual;

/**
 * Encapsulates a case and it's similarity to a query case.
 * 
 * @author sandro.emmenegger
 */
public class CaseMatch {
    
    private Individual caseInst;
    private SimResult similarityResult;
    
    private CaseMatch(){};
    
    public CaseMatch(Individual caseInst, SimResult similarityResult){
        this.caseInst = caseInst;
        this.similarityResult = similarityResult;
    }
    
    public String getCaseURI(){
        return caseInst.getURI();
    }
    
    public String getLocalName(){
        return caseInst.getLocalName();
    }
    
    public CBRCaseState getCaseState(){
        return CBRCase.getCaseState(this.caseInst);
    }

    public String getLabel(){
        String label = "";
        if (caseInst != null){
            label = caseInst.getLabel(null);
            if (label == null){
                label = caseInst.getURI();
            }
        }
        return label;
    }
    
    public SimResult getSimilarityResult(){
        return similarityResult;
    }
}
