/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.local.taxonomy.utils;

import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility for calculating instance class depth in an ontolgy.
 *
 * -> TODO: Extend to support ontology structures with multiple
 * inheritance/subclasses
 *
 * @author sandro.emmenegger
 */
public class OntTools {

    /**
     * Determine the lowest common ancestor for the classes.
     *
     * @param class1
     * @param class2
     * @return
     */
    public static OntClass getLCA(OntClass class1, OntClass class2) {
        List<OntClass> histClass1 = new ArrayList<>();
        histClass1.add(class1);
        histClass1 = getSuperClasses(class1, histClass1);
        List<OntClass> histClass2 = new ArrayList<>();
        histClass2.add(class2);
        histClass2 = getSuperClasses(class2, histClass2);
        for (OntClass ontClass1 : histClass1) {
            if (histClass2.contains(ontClass1)) {
                return ontClass1;
            }
        }
        return null;

    }

    protected static List<OntClass> getSuperClasses(OntClass cl, List<OntClass> hist) {
        OntClass superClass = cl.getSuperClass();
        if (superClass == null) {
            return hist;
        }
        hist.add(superClass);
        return getSuperClasses(superClass, hist);
    }

    /**
     * Searchs for the lowest common ancestor (LCA) in the instance hierarchy of
     * the query (source) and target instances. The parent/child resp.
     * child/parent property is considered to naviagte through the instances
     * hierarchy.      *
     * @param instance1
     * @param instance2
     * @param recursiveObjectProperty
     * @param expressesChildOf
     * @return
     */
    public static Individual getLCA(Individual instance1, Individual instance2, OntProperty recursiveObjectProperty, boolean expressesChildOf) {
        
        //get parents of instanc1
        List<Individual> histInstances1 = new ArrayList<>();
        histInstances1.add(instance1);
        List<Individual> duplciatesCheck1 = new ArrayList<>(histInstances1);
        histInstances1.addAll(getParentInstances(instance1, duplciatesCheck1, recursiveObjectProperty, expressesChildOf));
        
        //get parents of instanc2
        List<Individual> histInstances2 = new ArrayList<>();
        histInstances2.add(instance2);
        List<Individual> duplciatesCheck2 = new ArrayList<>(histInstances2);
        histInstances2.addAll(getParentInstances(instance2, duplciatesCheck2, recursiveObjectProperty, expressesChildOf));
        for (Individual histInstance1 : histInstances1) {
            if (histInstances2.contains(histInstance1)) {
                return histInstance1;
            }
        }
        return null;

    }

    private static List<Individual> getParentInstances(Individual instance, List<Individual> duplciatesCheck, OntProperty recursiveObjectProperty, boolean expressesChildOf) {
        List<Individual> instances = new ArrayList<>();
        ExtendedIterator parentInstances;
        if (expressesChildOf) {
            parentInstances = instance.listPropertyValues(recursiveObjectProperty);
        } else {
            parentInstances = OntAO.getInstance().getOntModel().listResourcesWithProperty(recursiveObjectProperty, instance);
        }
        while (parentInstances.hasNext()) {
            Individual parentInstance = ((OntResource) parentInstances.next()).asIndividual();
            if (!duplciatesCheck.contains(parentInstance)) {
                duplciatesCheck.add(parentInstance);
                instances.add(parentInstance);
                instances.addAll(getParentInstances(parentInstance, duplciatesCheck, recursiveObjectProperty, expressesChildOf));
            }
        }

        return instances;
    }

    /**
     * Return depth of a given class in the subclass structure of a given root
     * class. Returns null if class is cannot be found in the hierarchy of the
     * root class.
     *
     * @param depth (
     * @param root
     * @param resource
     * @return
     */
    public static Integer depth(OntClass root, OntClass resource) {
        if (root == null || resource == null) {
            return null;
        }
        return calculateDepth(root, resource, 1);
    }

    protected static Integer calculateDepth(OntClass parent, OntClass resource, int depth) {
        if (parent.equals(resource)) {
            return new Integer(depth);
        }

        Integer retVal = null;
        ExtendedIterator<OntClass> subclasses = parent.listSubClasses();
        while (subclasses.hasNext()) {
            OntClass ontClass = subclasses.next();
            retVal = calculateDepth(ontClass, resource, depth + 1);
            if (retVal != null) {
                break;
            }
        }
        return retVal;
    }

    public static Integer maxDepth(OntClass root) {
        if (root == null) {
            return null;
        }
        return calculateMaxDepth(root, 1);
    }

    protected static Integer calculateMaxDepth(OntClass parent, int depth) {
        if (!parent.hasSubClass()) {
            return new Integer(depth);
        }
        ExtendedIterator<OntClass> subclasses = parent.listSubClasses();
        Integer maxDepth = null;
        while (subclasses.hasNext()) {
            OntClass ontClass = subclasses.next();
            Integer temp = calculateMaxDepth(ontClass, depth + 1);
            if (maxDepth == null || temp > maxDepth) {
                maxDepth = temp;
            }
        }
        return maxDepth;
    }

}
