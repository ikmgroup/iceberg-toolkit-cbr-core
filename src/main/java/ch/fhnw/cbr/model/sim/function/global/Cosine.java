/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.global;

import ch.fhnw.cbr.model.sim.ObjectSimResult;
import ch.fhnw.cbr.model.sim.SimResult;
import ch.fhnw.cbr.model.sim.WeightedDouble;
import ch.fhnw.cbr.model.sim.function.GlobalSimilarityFunction;
import java.util.List;

/**
 * Calculates the cosine similarity of class instance similarties. 
 * Cannot be applied for properties of a class instances.
 *
 * @author sandro.emmenegger
 */
public class Cosine implements GlobalSimilarityFunction{

    
    /**
     * Calculates the cosine similarity out of the similarites of compared
     * query and target instances. 
     * 
     * Ex. comparing 2 query instances {q1, q2} with 3 targetInstances {t1, t2, t3}:
     * 
     * Expexted matrix passed to this method:
     * 
     *          query vectors                           target vectors
     * _____________________________    ___________________________________________
     *|                             |  |                                           |
     *      q1              q2              t1            t2              t3
     * q1 | sim(q1,q1)   sim(q2,q1)     sim(t1,q1)      sim(t2,q1)     sim(t3,q1)   
     * q2 | sim(q1,q2)   sim(q2,q2)     sim(t1,q2)      sim(t2,q2)     sim(t3,q2)
     * t1 | sim(q1,t1)   sim(q2,t1)     sim(t1,t1)      sim(t2,t1)     sim(t3,t1)
     * t2 | sim(q1,t2)   sim(q2,t2)     sim(t1,t2)      sim(t2,t2)     sim(t3,t2)
     * t3 | sim(q1,t3)   sim(q2,t3)     sim(t1,t3)      sim(t2,t3)     sim(t3,t3)
     * 
     * SIMILARITY =   (((sim(q1,q2)+sim(q2,q1))/wq * (sim(t1,q1)+sim(t2,q1) + sim(t3,q1))/wt + ...  
     *              / Sqr( ( ((sim(q1,q1)+sim(q2,q1))/wq )^2 + .... ) * Sqr( ((sim(t1,t2) ... 
     * 
     * where wq is the normalized weight, means in our case = 1/2, since each query instances is weighted equaly.
     * Dito for the target instances weight, means in our case = 1/3 since each target instance is weighted equaly.
     * 
     * @param simMatrix
     * @param querySize
     * @return 
     */
    @Override
    public ObjectSimResult computeObjectPropertySimilarity(SimResult[][] simMatrix, int querySize) {
        ObjectSimResult simResult = new ObjectSimResult(null, null);
        double[] queryVector = new double[simMatrix.length];
        for(int col=0; col < querySize; col++){
            for(int row=0; row<simMatrix.length; row++){
                queryVector[row] += simMatrix[col][row] != null ? simMatrix[col][row].getSimilarityValue()/querySize : 0;
                simResult.getSubResults().add(simMatrix[col][row]);
            }
        }
        
        
        double[] targetVector = new double[simMatrix.length];
        int targetSize = simMatrix.length - querySize;
        for(int col=querySize; col < simMatrix.length; col++){
            for(int row=0; row<simMatrix.length; row++){
                targetVector[row] += simMatrix[col][row] != null ? simMatrix[col][row].getSimilarityValue()/targetSize : 0;
                simResult.getSubResults().add(simMatrix[col][row]);
            }
        }
        
        double productSum = 0;
        double querySquareSums = 0;
        double targetSquareSums = 0;
        for(int i=0; i<queryVector.length; i++){
            productSum += queryVector[i]*targetVector[i];
            querySquareSums += queryVector[i]*queryVector[i];
            targetSquareSums += targetVector[i]*targetVector[i];
        }
        double similarity = productSum/(Math.sqrt(querySquareSums)*Math.sqrt(targetSquareSums));
        simResult.setSimilarityValue(similarity);
        return simResult;
    }

    @Override
    public double computeLocalSimilarities(List<WeightedDouble> values, boolean normalize) {
        throw new UnsupportedOperationException("Cosine does not support local similarity aggregation."); 
    }
    
}
