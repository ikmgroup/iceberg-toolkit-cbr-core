/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim;

/**
 *  Value object combining a double value and corresponding weight.
 *
 * @author sandro.emmenegger
 */
public class WeightedDouble {
    
    private double value;
    private double weight;
    
    private WeightedDouble(){};
    
    public WeightedDouble(double value, double weight){
        this.value = value;
        this.weight = weight;
    }

    /**
     * @return the value
     */
    public double getValue() {
        return value;
    }

    /**
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }
}
