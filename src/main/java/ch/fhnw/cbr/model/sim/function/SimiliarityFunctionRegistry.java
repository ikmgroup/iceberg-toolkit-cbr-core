/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function;

import ch.fhnw.cbr.model.sim.function.global.Average;
import ch.fhnw.cbr.model.sim.function.global.Cosine;
import ch.fhnw.cbr.model.sim.function.global.Probabilistic;
import ch.fhnw.cbr.model.sim.function.local.Equals;
import ch.fhnw.cbr.model.sim.function.local.JaroWinkler;
import ch.fhnw.cbr.model.sim.function.local.Levenshtein;
import ch.fhnw.cbr.model.sim.function.local.SoftTFIDFJaroWinkler;
import ch.fhnw.cbr.model.sim.function.local.Version;
import ch.fhnw.cbr.model.sim.function.local.taxonomy.TaxonomyAnnotatedValue;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sandro.emmenegger
 */
public final class SimiliarityFunctionRegistry {
    
    private static final Map<String, Class> globalFunctions = new HashMap<>();
    private static final Map<String, Class> localFunctions = new HashMap<>();
    private static final Map<String, Class> taxonomyFunctions = new HashMap<>();
    
    static {
        //global functions
        globalFunctions.put("average", Average.class);
        globalFunctions.put("probabilistic", Probabilistic.class);
        globalFunctions.put("cosine", Cosine.class);
        //local functions
        localFunctions.put("equals", Equals.class);
        localFunctions.put("levenshtein", Levenshtein.class);
        localFunctions.put("jaroWinkler", JaroWinkler.class);
        localFunctions.put("softTFIDFJaroWinkler", SoftTFIDFJaroWinkler.class);
        localFunctions.put("version", Version.class);
        
//        taxonomyFunctions.put("taxonomyDistance", TaxonomyDistance.class);
        taxonomyFunctions.put("taxonomicNearestCommonPredecessor", TaxonomyAnnotatedValue.class);
    }
    
    public static GlobalSimilarityFunction getGlobalFunction(String shortName){
        GlobalSimilarityFunction obj = null;
        if (globalFunctions.containsKey(shortName)){
            Class functionClass = globalFunctions.get(shortName);
            try {
                obj = (GlobalSimilarityFunction) functionClass.newInstance();
            } catch (    InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(SimiliarityFunctionRegistry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return obj;
    }    
    
    
    public static LocalSimilarityFunction getLocalFunction(String shortName){
        LocalSimilarityFunction obj = null;
        if (localFunctions.containsKey(shortName)){
            Class functionClass = localFunctions.get(shortName);
            try {
                obj = (LocalSimilarityFunction) functionClass.newInstance();
            } catch (    InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(SimiliarityFunctionRegistry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return obj;
    }
    
        public static TaxonomySimilarityFunction getTaxonomyFunction(String shortName){
        TaxonomySimilarityFunction obj = null;
        if (taxonomyFunctions.containsKey(shortName)){
            Class functionClass = taxonomyFunctions.get(shortName);
            try {
                obj = (TaxonomySimilarityFunction) functionClass.newInstance();
            } catch (    InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(SimiliarityFunctionRegistry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return obj;
    }


    public static boolean containsGlobalFunction(String simFunctionName) {
        return globalFunctions.containsKey(simFunctionName);
    }
    
    public static boolean containsLocalFunction(String simFunctionName) {
        return localFunctions.containsKey(simFunctionName);
    }
    
    public static boolean containsTaxonomyFunction(String simFunctionName) {
        return taxonomyFunctions.containsKey(simFunctionName);
    }
    
}
