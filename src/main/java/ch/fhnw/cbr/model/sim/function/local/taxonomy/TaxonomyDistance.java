/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.local.taxonomy;

import ch.fhnw.cbr.model.sim.SimResult;
import ch.fhnw.cbr.model.sim.function.local.taxonomy.utils.OntTools;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import ch.fhnw.cbr.model.sim.function.TaxonomySimilarityFunction;

/**
 * Calculates the distance of two instances in the taxonomy to:
 * 
 * E. Agirre, X. Arregi, X. Artola, A.D. de I.K.S., Conceptual Distance and Automatic Spelling Correction. 
 * Available at: http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.43.9218 [Accessed April 4, 2014].
 * 
 * The distance is a value > 0 and will be normalized to a similarity value between 0 and 1 based on the formula:
 * 
 * similarity = 1-((sourcePathSerie + targetPathSerie)/(2*maxDepthSerie))
 * 
 * where sourcePathSerie = harmonic serie from source taxonomy (distance from deepest set value to the least common ancestor of soure and target)
 * dito for the targetPathSerie
 * 
 * maxDepthSerie = the max class taxonomy depth + the depth of the defined property hierarchy.
 * 
 * @author sandro.emmenegger
 */
public class TaxonomyDistance{
    
    public double compute(Individual sourceInstance, Individual targetInstance, OntClass rootClass, SimResult[] properties) {
        
        //calculate level of source and target class of instances
        Integer sourceDepth = OntTools.depth(rootClass, sourceInstance.getOntClass());
        Integer targetDepth = OntTools.depth(rootClass, targetInstance.getOntClass());
        if (sourceDepth == null || targetDepth == null){
            return 0.0;
        }        
        
        //determine the lowest common ancestor for classes 
        OntClass lowestCommonAncestor = OntTools.getLCA(sourceInstance.getOntClass(), targetInstance.getOntClass());
        Integer lcaDepth = OntTools.depth(rootClass, lowestCommonAncestor);
        if (lcaDepth == null){
            return 0.0;
        }
        
        Integer maxClassDepth = OntTools.maxDepth(rootClass);
        if(sourceDepth < maxClassDepth && sourceDepth.equals(lcaDepth)){
            return 1.0;
        }
        
        Integer maxDepth = maxClassDepth + properties.length;
        
        if (lcaDepth.equals(maxClassDepth)){ //compare further property hierarchy only if compared instances of same deepest class
            
            if(properties.length == 0){
                return 1.0;
            }

            for(SimResult propertySim : properties){
                if(propertySim == null || propertySim.isSourceNull()){
                    return 1.0;
                }
 
                if ((new Double(1.0)).equals(propertySim.getSimilarityValue())){
                    lcaDepth++;
                }else{
                    break;
                }
            }
        }
        
        if(lcaDepth.equals(maxDepth)){
            return 1.0;
        }
        
        //calculate similarity based on taxonomy normalized with max depth harmonic series sum
        double lcaDepthSerie = (2*(harmonicSeries(lcaDepth+1, maxDepth))+1.0/lcaDepth);
        double maxDepthSerie = (2*harmonicSeries(2, maxDepth))+1;
        double similarity = 1-( lcaDepthSerie/maxDepthSerie );
        
        return similarity;
    }
    
    /**
     * Calculate harmonic serie sum between start and end;
     * 
     * @param start
     * @param end
     * @return 
     */
    private double harmonicSeries(int start, int end) {
        double serieSum = 0.0;        
        for(int i=start;i<end;i++){
            serieSum += 1.0/i;
        }
        return serieSum;
    }
   
}
