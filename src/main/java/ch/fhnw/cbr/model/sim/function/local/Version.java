/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.local;

import ch.fhnw.cbr.model.sim.function.AbstractLocalSimilarityFunction;

/**
 * Compares two version strings. Separates the version parts by any non alphanumeric 
 * character. 
 * The function is asymmetric. If the value in the query fully contains the value in the case, 
 * the similarity is 1 - 1/d. Othwerwise the similarity is calculated based on the depth of splited 
 * version parts when they first differ and on the a difference value calculated for this first
 * non equal version parts:
 * 
 * Sim: 1.0 - ((1.0/(amountOfEqualVersionParts+1))*difference(sourceVersionPart, targetVersionPart);
 * 
 * Ex. sim("1.3", "1.3.7") = 1.0
 *     sim("1.3.2", "1.2.4") = 
 * 
 * @author sandro.emmenegger
 */
public class Version extends AbstractLocalSimilarityFunction {

    @Override
    public double compute(Object source, Object target) {

        if (source == target) return 1.0;
        if(source == null || target == null) return 0.0;
        if (source.equals(target)) return 1.0;
    
        // split both strings by non-alphanumerical characters
        String[] sourceSplit = source.toString().trim().split("[^a-zA-Z0-9']+");
        String[] targetSplit = target.toString().trim().split("[^a-zA-Z0-9']+");
        
        // compare from left to right
        for (int i = 0; i < sourceSplit.length; i++) {
            
            String sourceEl = sourceSplit[i];
            
            // similarity function is asymmetric: if the value in the query 
            // fully contains the value in the case, the similarity is 1 - 1/d
            if(i >= targetSplit.length)return 1.0d - (1.0d/(double)(i+1));
            else{
                String targetEl = targetSplit[i];
                
                // we compute the difference when the two values first differ
                // from each other:
                if(!sourceEl.equalsIgnoreCase(targetEl)){
                    return 1.0d - ((1.0d/(double)(i+1))*difference(sourceEl, targetEl));
                }
            }
        }
        
        // if the value in the case (e.g. 1.2.1) fully contains the value in the query (i.e.
        // the query is more general (e.g. 1.2), then the similarity is 1.
        return 1.0d;
    }
    
    /**
     * Calculate a normalized simialrity based on the difference of two alphanumeric 
     * values if the letters are considered to extend the base 10 by 26 to a base 36 of each 
     * character position in the string.
     * 
     * @param sourceEl
     * @param targetEl
     * @return 
     */
    private double difference(String sourceEl, String targetEl) {
        long sourceNumber = stringAsNumber(sourceEl);
        long targetNumber = stringAsNumber(targetEl);
        double max = Math.max(sourceNumber, targetNumber);
        double retVal = Math.abs(sourceNumber-targetNumber)/max;
        return retVal;
    }


    /**
     * Converts a alphanumeric string into a number with the base 36 (26 letters + 10 numbers).
     * 
     * @param s
     * @return 
     */
    private long stringAsNumber(String s){
        int exponent = 0;
        long ret = 0;
        for(int i = s.length()-1; i>=0 ;i--){
            int n = s.toLowerCase().charAt(i);
            
            //handle numbers 0-9
            int curValue;
            if(n >=48 && n<=57)curValue = n - 48;
            else{ //handle letters
                curValue = n -87; 
            }
            ret += curValue * (int)Math.pow(35.0d, (double)exponent);
            exponent++;
       }
        return ret;
    }
}
