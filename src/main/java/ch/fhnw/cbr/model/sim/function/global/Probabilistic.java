/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.global;

import ch.fhnw.cbr.model.sim.ObjectSimResult;
import ch.fhnw.cbr.model.sim.SimResult;
import ch.fhnw.cbr.model.sim.WeightedDouble;
import ch.fhnw.cbr.model.sim.function.GlobalSimilarityFunction;
import java.util.ArrayList;
import java.util.List;

/**
 * Aggregates the similarity values in a probabilistc way.
 * Can be applied to class instance similarity values as well as property similarity values.
 * 
 * @author sandro.emmenegger
 */
public class Probabilistic implements GlobalSimilarityFunction{
    
    public final static double SMOOTHING = 0.001;
    
    @Override
    public double computeLocalSimilarities(List<WeightedDouble> values, boolean normalize){
        
        assert values != null : "Null value for value aggregation not allowed.";
        return calculate(normalize, values);
    }

    /**
     * Calculates one probibilistic similarity out of the similarites of compared
     * query and target instances. Considers only the highest (max) similarity value
     * of the target instances.
     * 
     * Ex. comparing 2 query instances {q1, q2} with 3 targetInstances {t1, t2, t3}:
     * 
     * Expexted matrix passed to this method:
     * 
     *          query vectors                           target vectors
     * _____________________________    ___________________________________________
     *|                             |  |                                           |
     *      q1              q2              t1            t2              t3
     * q1 | sim(q1,q1)   sim(q2,q1)     sim(t1,q1)      sim(t2,q1)     sim(t3,q1)   
     * q2 | sim(q1,q2)   sim(q2,q2)     sim(t1,q2)      sim(t2,q2)     sim(t3,q2)
     * t1 | sim(q1,t1)   sim(q2,t1)     sim(t1,t1)      sim(t2,t1)     sim(t3,t1)
     * t2 | sim(q1,t2)   sim(q2,t2)     sim(t1,t2)      sim(t2,t2)     sim(t3,t2)
     * t3 | sim(q1,t3)   sim(q2,t3)     sim(t1,t3)      sim(t2,t3)     sim(t3,t3)
     * 
     * SIMILARITY = max(sim(q1,t1), sim(q1,t2), sim(q1,t3))^w * max(sim(q1,t1), sim(q1,t2), sim(q1,t3))^w
     * 
     * where w is the normalized weight, means in our case = 1/2, since each query instances is weighted equaly.
     * 
     * @param simMatrix similarity values of all compared instances.
     * @param querySize amount of query instances
     * @return 
     */
    @Override
    public ObjectSimResult computeObjectPropertySimilarity(SimResult[][] simMatrix, int querySize) {
        ObjectSimResult simResult = new ObjectSimResult(null, null);
        
        List<WeightedDouble> maxTargetValues = new ArrayList<>();
        for(int col=0; col < querySize; col++){
            SimResult maxValue = null;
            for(int row=querySize; row<simMatrix.length; row++){
                if (maxValue==null){
                    maxValue = simMatrix[col][row];
                }else{
                    if (simMatrix[col][row].getSimilarityValue() > maxValue.getSimilarityValue()){
                        maxValue = simMatrix[col][row];
                    }
                }
                simResult.getSubResults().add(simMatrix[col][row]);
            }
            if(maxValue != null){
                maxTargetValues.add(new WeightedDouble(maxValue.getSimilarityValue(), 1));
                maxValue.setComment(maxValue.getComment() +"max");
                
            }
        }
        simResult.setSimilarityValue(computeLocalSimilarities(maxTargetValues, true));
        return simResult;
    }
    
   
    /**
     * Aggregates the similarity values in probabilistic way:
     * 
     * SIMILARITY = sim1^w1 * sim2^w2 * ... * simN^wN
     * 
     * where w is the (normalized) weight
     * 
     * @param normalize
     * @param values
     * @return 
     */
    private double calculate(boolean normalize, List<WeightedDouble> values) {
        double retVal = 0.0;
        double weightSum = 0.0;
        if(normalize){
            for (WeightedDouble value : values) {
                weightSum += value.getWeight();
            }
        }
        for (WeightedDouble value : values) {
            double val = value.getValue() <= 0.0 ? SMOOTHING : value.getValue();
            double tempVal;
            
            if(normalize){
                tempVal = Math.pow(val, (value.getWeight()/weightSum));
            }
            else{
                tempVal = Math.pow(val, value.getWeight());
            }
            
            if (retVal == 0.0){
                retVal = tempVal;
            }
            else{
                retVal *= tempVal;
            }
        }
        return retVal;
    }
    
}
