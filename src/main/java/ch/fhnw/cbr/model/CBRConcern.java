/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author sandro.emmenegger
 */
public class CBRConcern extends CBRBaseClass {
    
    public static final String CLASS_ID = CBR_NS.CBR+"Concern";
    
    private static final Logger LOG = Logger.getLogger(CBRConcern.class.getName());
        
    private List<CBRRole> belongsToRoles;

    public CBRConcern(String uri){
        super(uri);
    }
    
    public CBRConcern(Individual individual){
        super(individual);
    }
    
    public void setBelongsToRoles(List<CBRRole> belongsToRoles) {
        this.belongsToRoles = belongsToRoles;
    }
    
    public List<CBRRole> getBelongsToRoles() {
        if (belongsToRoles == null) {
            belongsToRoles = new ArrayList<>();

            String queryString
                    = "PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                    + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                    + "PREFIX cbr:  <http://ikm-group.ch/cbr#>\n"
                    + "SELECT ?role WHERE {\n"
                    + "   ?concern cbr:concernsBelongToRole ?role. \n"
                    + "}\n";

            ParameterizedSparqlString queryStr = new ParameterizedSparqlString(queryString);
            queryStr.setParam("concern", getIndividual());
            Query query = QueryFactory.create(queryStr.toString());
            QueryExecution qexec = QueryExecutionFactory.create(query, OntAO.getInstance().getOntModel());
            ResultSet resSet = qexec.execSelect();

            while (resSet.hasNext()) {
                QuerySolution querySolution = resSet.next();
                Resource res = querySolution.getResource("role");
                CBRRole role = new CBRRole(res.getURI());
                belongsToRoles.add(role);
            }
        }
        
        return belongsToRoles;
    }    
}
