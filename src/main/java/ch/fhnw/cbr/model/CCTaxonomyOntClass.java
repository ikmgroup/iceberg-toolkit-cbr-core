/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.core.queries.QueryMap;
import ch.fhnw.cbr.model.sim.ObjectSimResult;
import ch.fhnw.cbr.model.sim.SimResult;
import ch.fhnw.cbr.model.sim.function.GlobalSimilarityFunction;
import ch.fhnw.cbr.model.sim.function.TaxonomySimilarityFunction;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a CBR characterization class in a taxonomy structure.
 * Similarity functions applied to instances of this class consider all the taxonomic position 
 * of the instances in the class or also instances hierarchy (recursive parent/child or child/parent property).
 *
 * @author sandro.emmenegger
 */
public class CCTaxonomyOntClass extends CCOntClass {

    private TaxonomySimilarityFunction taxonomySimFunction;
    private String taxonomySetId;
    private OntProperty recursiveObjectProperty;
    private boolean recursivePropertyExpressesChildOf;
    private Map<String, Double> taxonomyResourceValues = new HashMap<>();

    public CCTaxonomyOntClass(CBRCaseView caseView, 
                              OntClass ontClass, 
                              GlobalSimilarityFunction simFunctionName, 
                              TaxonomySimilarityFunction taxonomySimFunction, 
                              String taxonomySetId, 
                              OntProperty recursiveObjectProperty,
                              boolean recursivePropertyExpressesChildOf) {
        
        assert caseView != null : "View on case for CCResource must not be null.";
        assert ontClass != null : "Resource for CCResource must not be null.";
        assert simFunctionName != null;
        assert taxonomySimFunction != null;
        assert taxonomySetId != null;
        
        this.caseView = caseView;
        this.resource = ontClass;
        this.simFunction = simFunctionName;
        this.taxonomySimFunction = taxonomySimFunction;
        this.taxonomySetId = taxonomySetId;
        this.recursiveObjectProperty = recursiveObjectProperty;
        this.recursivePropertyExpressesChildOf = recursivePropertyExpressesChildOf;
        
        loadCCTaxonomyClassesAndInstances();
    }

    /**
     * Query for all taxonomy annotations of same set using this.taxonomySetId
     * fill map with key=resourceUri  and value = simValue for lookup when compari
     */
    private void loadCCTaxonomyClassesAndInstances() {
        String queryString = QueryMap.getQuery("getClassOrInstanceTaxonomyValues");
        ParameterizedSparqlString queryStr = new ParameterizedSparqlString(queryString);
        queryStr.setParam("caseView", this.caseView.getIndividual());
        queryStr.setLiteral("setId", this.taxonomySetId);
        Query query = QueryFactory.create(queryStr.toString());
        QueryExecution qexec = QueryExecutionFactory.create(query, OntAO.getInstance().getOntModel());
        ResultSet resSet = qexec.execSelect();
        while (resSet.hasNext()) {
            QuerySolution querySolution = resSet.next();
            Resource res = querySolution.getResource("resource");
            Double simValue = querySolution.getLiteral("simValue").getDouble();
            this.taxonomyResourceValues.put(res.getURI(), simValue);
        }
    }
    
    @Override
    public SimResult compare(Individual queryCaseInst, Individual caseInst) {

        assert queryCaseInst != null && caseInst != null : "Expect individuals to be compared not to be null";

        String simFunctionName = taxonomySimFunction.getClass().getSimpleName();
        ObjectSimResult similarityLog = new ObjectSimResult(resource.getURI(), simFunctionName);

        similarityLog.setSourceNull(false);
        similarityLog.setTargetNull(false);
        similarityLog.setSimilarityValue(taxonomySimFunction.compute(queryCaseInst, caseInst, taxonomyResourceValues, recursiveObjectProperty, recursivePropertyExpressesChildOf));

        return similarityLog;
    }    
}
