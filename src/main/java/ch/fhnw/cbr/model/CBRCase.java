/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.core.queries.QueryMap;
import ch.fhnw.cbr.model.sim.function.SimiliarityFunctionRegistry;
import ch.fhnw.cbr.persistence.OntAO;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import java.util.UUID;

/**
 *
 * @author sandro.emmenegger
 */
public class CBRCase extends CCOntClass{

    public CBRCase(CBRCaseView caseView){
        this.caseView = caseView;
        this.resource = null;
        this.simFunction = null;
        
        //set root case class (resource) and aggregation function.
        loadRootCaseSettings();
        
        if(resource == null){
            this.resource = OntAO.getInstance().getOntClass(CBR_NS.CBR+"Element");
        }
        if(this.simFunction == null){
            this.simFunction = SimiliarityFunctionRegistry.getGlobalFunction("average");
        }
        
        init();
    }
    
    public static void removeInstance(Individual i) {
    	i.removeProperties();
        i.remove();
    }
    
    /**
     * Searchs for the latest case state in the case state history. 
     * 
     * @param caseInstance
     * @return 
     */
    public static CBRCaseState getCaseState(Individual caseInstance){
        String queryString = "PREFIX cbr:  <http://ikm-group.ch/cbr#>\n"
                    + "SELECT ?caseState \n" +
                        "WHERE {\n" +
                        "    ?caseStateHist cbr:belongsToCase ?case .\n" +
                        "    ?caseStateHist cbr:caseStateTimestamp ?timestamp .\n" +
                        "    ?caseStateHist cbr:hasCaseState ?caseState\n" +
                        "}\n" +
                        "ORDER BY DESC(?timestamp)\n" +
                        "LIMIT 1";

            ParameterizedSparqlString queryStr = new ParameterizedSparqlString(queryString);
            queryStr.setParam("case", caseInstance);
            Query query = QueryFactory.create(queryStr.toString());
            QueryExecution qexec = QueryExecutionFactory.create(query, OntAO.getInstance().getOntModel());
            ResultSet resSet = qexec.execSelect();
            CBRCaseState state = null;
            if(resSet.hasNext()){
                QuerySolution querySolution = resSet.next();
                Resource res = querySolution.getResource("caseState");
                state = CBRCaseState.get(res.getURI());
            }
            return state;
    }

    /**
     * Extracts the case charactersiation space including the instances of the 
     * defined caseView.
     * 
     * @param caseView
     * @return 
     */
    public static Model getCaseCharacterisationModel(CBRCaseView caseView){
        
        String queryString = 
            "PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
            "PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>\n" + 
            "PREFIX sim:  <http://ikm-group.ch/similarity#>\n" +                 
            "CONSTRUCT{\n" +
            "	?subject ?property ?object .\n" +
            "	?property rdf:type ?propertyType .\n" +
            "	?property rdfs:domain ?domain .\n" +
            "    ?property rdfs:range ?range.\n" +
            "	?subject a ?domain .\n" +
            "    ?object a ?range\n" +
            "}\n" +
            "WHERE {\n" +
            "    ?subject ?property ?object .\n" +
            "	?property sim:similarity ?similarity .\n" +
            "	?similarity sim:belongsToCaseView ?caseView .\n" +
            "    ?property rdf:type ?propertyType .\n" +
            "    ?property rdfs:domain ?domain .\n" +
            "    ?property rdfs:range ?range.\n" +
            "}";
        
        ParameterizedSparqlString queryStr = new ParameterizedSparqlString(queryString);
        queryStr.setParam("caseView", caseView.getIndividual());

        Query query = QueryFactory.create(queryStr.toString());

        QueryExecution qexec = QueryExecutionFactory.create(query, OntAO.getInstance().getOntModel()) ;
        Model resultModel = qexec.execConstruct() ;
        qexec.close();
        return resultModel;
    }
    
    public Individual createNewInstance(String uri, String name){
        Individual instance = resource.createIndividual(uri);
        instance.setLabel(name, CBR.LANGUAGE);
        String caseStateHistUri = CBR.CONFIG.getString("data.namespace", "http://ikm-group.ch/cbr/data#") + "CaseStateHist_" + UUID.randomUUID().toString();
        new CBRCaseStateHist(caseStateHistUri, CBRCaseState.ADAPTATION_CASE_STATE, instance);
        return instance;
    }
    
    
    private void loadRootCaseSettings() {
        String queryString = QueryMap.getQuery("getRootCaseSimilarity");
        ParameterizedSparqlString queryStr = new ParameterizedSparqlString(queryString);
        queryStr.setParam("caseView", caseView.getIndividual());
        Query query = QueryFactory.create(queryStr.toString());
        QueryExecution qexec = QueryExecutionFactory.create(query, OntAO.getInstance().getOntModel());
        ResultSet resSet = qexec.execSelect();

        while (resSet.hasNext()) {
            QuerySolution querySolution = resSet.next();
            Resource res = querySolution.getResource("class");
            this.resource = res.as(OntClass.class);

            Resource aggregationFunctionRes = querySolution.getResource("aggregationFunction");
            this.simFunction = SimiliarityFunctionRegistry.getGlobalFunction(aggregationFunctionRes.getLocalName());
        }

        qexec.close();
    }

}
