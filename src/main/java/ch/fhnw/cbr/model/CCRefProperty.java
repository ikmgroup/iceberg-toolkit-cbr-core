/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.model.sim.ObjectPropertySimResult;
import ch.fhnw.cbr.model.sim.ObjectSimResult;
import ch.fhnw.cbr.model.sim.SimResult;
import ch.fhnw.cbr.model.sim.function.GlobalSimilarityFunction;
import ch.fhnw.cbr.model.sim.function.SimiliarityFunctionRegistry;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.RDFNode;
import java.util.List;

/**
 * Represents an object property or an edge in the ontology.
 *
 * @author sandro.emmenegger
 */
public class CCRefProperty extends CCNode<OntProperty>{
    
    private CCOntClass refResource;
    private boolean newInstancesAllowed = false;

    /** Global similarity function (aggregation function) */
    private GlobalSimilarityFunction aggregationSimFunction;
    
    public CCRefProperty(CBRCaseView context, OntProperty ontProperty, String aggregationSimFunction, float weight, boolean allowNewInstances){
        assert context != null : "Context for CCRefProperty must not be null.";
        assert ontProperty != null : "Resource for CCRefProperty must not be null.";
        assert weight >= 0.0 : "Weight must be positive: " + weight;
        assert SimiliarityFunctionRegistry.containsGlobalFunction(aggregationSimFunction) 
                : "Global similarity function not registered: "+aggregationSimFunction;
        
        this.resource = ontProperty;
        this.aggregationSimFunction = SimiliarityFunctionRegistry.getGlobalFunction(aggregationSimFunction);
        this.newInstancesAllowed = allowNewInstances;
        setSimilarityWeight(weight);
        
        OntResource refRes = ontProperty.getRange();
        OntClass refClass = refRes.as(OntClass.class);

        refResource = new CCOntClass(context, refClass, this.aggregationSimFunction);
    }
    
    public CCRefProperty(CBRCaseView context, 
                         OntProperty ontProperty, 
                         String aggregationSimFunction, 
                         float weight, 
                         boolean allowNewInstances, 
                         String taxonomySimFunction, 
                         String taxonomySetId, 
                         OntProperty recursiveObjectProperty,
                         boolean recursivePropertyExpressesChildOf){
        assert context != null : "Context for CCRefProperty must not be null.";
        assert ontProperty != null : "Resource for CCRefProperty must not be null.";
        assert weight >= 0.0 : "Weight must be positive: " + weight;
        assert SimiliarityFunctionRegistry.containsGlobalFunction(aggregationSimFunction) : "Global similarity function not registered: "+aggregationSimFunction;
        assert SimiliarityFunctionRegistry.containsTaxonomyFunction(taxonomySimFunction) : "Taxonomy similarity function not registered: " +taxonomySimFunction;
        assert taxonomySetId != null : "Taxonomy set id must not be null.";
        
        this.resource = ontProperty;
        this.aggregationSimFunction = SimiliarityFunctionRegistry.getGlobalFunction(aggregationSimFunction);
        this.newInstancesAllowed = allowNewInstances;
        setSimilarityWeight(weight);
        
        OntResource refRes = ontProperty.getRange();
        OntClass refClass = refRes.as(OntClass.class);

        refResource = new CCTaxonomyOntClass(context, refClass, this.aggregationSimFunction, SimiliarityFunctionRegistry.getTaxonomyFunction(taxonomySimFunction), taxonomySetId, recursiveObjectProperty, recursivePropertyExpressesChildOf);
    }    
    
    public boolean newInstancesAllowed(){
        return this.newInstancesAllowed;
    }

    @Override
    public SimResult compare(Individual queryCaseInst, Individual caseInst) {
        
        ObjectPropertySimResult retVal = new ObjectPropertySimResult(resource.getURI(), null);
       
        //calculate similarity of relational properties (object properties)
        List<RDFNode> queryCaseRefIstances = queryCaseInst.listPropertyValues(resource).toList();
        
        int querySize = queryCaseRefIstances.size();
        List<RDFNode> caseRefIstances = caseInst.listPropertyValues(resource).toList();
        int totalInstances = querySize + caseRefIstances.size();
        SimResult[][] simMatrix = new SimResult[totalInstances][totalInstances];
        for(int col=0; col<totalInstances; col++){
            for(int row=0; row<totalInstances; row++){
                if (row==col){
                    simMatrix[col][row] = new ObjectSimResult(refResource.getURI(), null);
                    simMatrix[col][row].setSimilarityValue(1.0);
                    simMatrix[col][row].setComment(" (col="+row+" row="+col+")");
                }
                if (row<col){ //copy value from upper right matrix triangle
                    simMatrix[col][row]=simMatrix[row][col].copy();
                    simMatrix[col][row].setComment(" (col="+col+" row="+row+") copied (from col="+row+" row="+col+")");
                }
                if (row>col){
                    Individual leftInstance = col<querySize ? ((OntResource)queryCaseRefIstances.get(col)).asIndividual() : ((OntResource)caseRefIstances.get(col-querySize)).asIndividual();
                    Individual rightInstance = row<querySize ? ((OntResource)queryCaseRefIstances.get(row)).asIndividual() : ((OntResource)caseRefIstances.get(row-querySize)).asIndividual();
                    SimResult refSimValue = getRefResource().compare(leftInstance, rightInstance);
                    refSimValue.setComment(" left: "+leftInstance.getLocalName()+" (col="+col+") right: "+rightInstance.getLocalName()+" (row="+row+") ");
                    simMatrix[col][row]= refSimValue;
                    if (!refSimValue.isSourceNull()) retVal.setSourceNull(false);
                    if (!refSimValue.isTargetNull()) retVal.setTargetNull(false);
                }
            }
        }
        
        if(!retVal.isSourceNull() || !retVal.isTargetNull()){
            ObjectSimResult tempResult = aggregationSimFunction.computeObjectPropertySimilarity(simMatrix, querySize);    
            retVal.setSimilarityValue(tempResult.getSimilarityValue());
            retVal.setSubResults(tempResult.getSubResults());
        }
        
        return retVal;
    }

    public CCOntClass getRefResource() {
        return refResource;
    }
    
    @Override
    public String printCaseCharactersiation(Individual instance, int level) {
        StringBuilder retVal = new StringBuilder();
        NodeIterator caseRefIstancesItr = instance.listPropertyValues(resource);
        while (caseRefIstancesItr.hasNext()) {
            Individual caseRefIstances = ((OntResource)caseRefIstancesItr.next()).asIndividual();
            String temp = getRefResource().printCaseCharactersiation(caseRefIstances, level);
            if (temp != null && !"".equals(temp.trim())){
                retVal.append(temp).append("\n");
            }
        }
        
        return retVal.toString();
    }        
}
