/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.core.config;

/**
 * Represents ISO 639-1 (alpha2) and ISO 639-2/T language codes.
 * http://www.loc.gov/standards/iso639-2/php/code_list.php
 * 
 * @author sandro.emmenegger
 */
public enum Language {
    
    ENGLISH("en", "eng"),
    GERMAN("de", "deu");
   
    private final String isoCode1;
    private final String isoCode2;
    
    private Language(String isoCode1, String isoCode2){
        this.isoCode1 = isoCode1;
        this.isoCode2 = isoCode2;
    }

    public String getIsoCode1() {
        return isoCode1;
    }

    public String getIsoCode2() {
        return isoCode2;
    }
    
    public String getIsoCode1UpperCase() {
        return isoCode1;
    }

    public String getIsoCode2UpperCase() {
        return isoCode2;
    }
    
}
