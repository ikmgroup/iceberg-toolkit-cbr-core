/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.core.config;

import ch.fhnw.cbr.model.CBR_NS;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * Encapsulates a combined CBR component configuration based on one or more
 * properties configuration(s).
 *
 * @author sandro.emmenegger
 */
public class CBR {

    public static Configuration CONFIG;
    
    public static String LANGUAGE = Language.ENGLISH.getIsoCode1UpperCase();
    public static String ALTERNATIVE_LANGUAGE = Language.GERMAN.getIsoCode1UpperCase();
    public static final String RDFS_LABEL = "http://www.w3.org/2000/01/rdf-schema#label";
    public static final String RDFS_COMMENT = "http://www.w3.org/2000/01/rdf-schema#comment";    
    public static final String DATA_TYPE_PROPERTY_URI = CBR_NS.OWL + "DatatypeProperty";
    public static final String OBJECT_TYPE_PROPERTY_URI = CBR_NS.OWL + "ObjectProperty";
    public static final String OWL_CLASS_URI = CBR_NS.OWL + "Class";
    
    public static final String DEAFAULT_PROPERTY_FILE = "component.properties";
    
    private CBR() {
    }

    static {
        try {
            load(DEAFAULT_PROPERTY_FILE);
        } catch (ConfigurationException ex) {
            Logger.getLogger(CBR.class.getName()).log(Level.INFO, "Failure when loading default application properties for cbr component.", ex);
        }
    }

    private static void load(String propertyFile) throws ConfigurationException {
        CONFIG = new PropertiesConfiguration(propertyFile);
    }

    public static synchronized void setLANGUAGE(String language) {
        LANGUAGE = language;
    }

    public static synchronized void setALTERNATIVE_LANGUAGE(String altLanguage) {
        ALTERNATIVE_LANGUAGE = altLanguage;
    }
}
