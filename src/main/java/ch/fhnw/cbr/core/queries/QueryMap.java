/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.core.queries;

import ch.fhnw.cbr.core.config.CBR;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sandro.emmenegger
 */
public class QueryMap {

    private static final Map<String, String> queries = new HashMap<>();

    static {
        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(QueryMap.class.getResourceAsStream(CBR.CONFIG.getString("queries.file", "/queries/queries.txt"))));
            String queryName = "";
            String comment = "";
            StringBuilder queryStr = new StringBuilder();
            for (String line; (line = br.readLine()) != null;) {
                if (line.startsWith("##START##")) {
                    queryName = line.substring(line.indexOf(":") + 1);
                } else {
                    if (line.startsWith("##END##")) {
                        queries.put(queryName, queryStr.toString());
                        queryName = "";
                        comment = "";
                        queryStr = new StringBuilder();
                    } else {
                        if (line.startsWith("##COMMENT")) {
                            comment = line.substring(line.indexOf(":") + 1);
                        } else {
                            queryStr.append(line).append("\n");
                        }
                    }
                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(QueryMap.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(QueryMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String getQuery(String name) {
        return queries.get(name);
    }

}
