/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.local;

import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.core.config.Language;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author sandro.emmenegger
 */
public class SoftTFIDFJaroWinklerTest {
    
    @BeforeClass
    public static void init(){
        CBR.setLANGUAGE(Language.ENGLISH.getIsoCode1UpperCase());
    }
    
    /**
     * Test of compute method, of class SoftTFIDFJaroWinkler.
     */
    @Test
    public void testCompute() {
        System.out.println("compute");
        OntClass ontClass = OntAO.getInstance().getOntModel().getOntClass("http://ikm-group.ch/cbr/cbr-unittest#BusinessRole");
        OntProperty property = OntAO.getInstance().getOntModel().createAnnotationProperty("http://www.w3.org/2000/01/rdf-schema#label");
        SoftTFIDFJaroWinkler instance = new SoftTFIDFJaroWinkler();
        instance.prepare(ontClass, property);        
        
        String source = "Program Manager";
        String target = "Program Manager";
        double result = instance.compute(source, target);
        assertEquals(1.0, result, 0.05);
        
    }
    
    @Test
    public void testCompute2() {
        System.out.println("compute");
        OntClass ontClass = OntAO.getInstance().getOntModel().getOntClass("http://ikm-group.ch/cbr/cbr-unittest#BusinessRole");
        OntProperty property = OntAO.getInstance().getOntModel().createAnnotationProperty("http://www.w3.org/2000/01/rdf-schema#label");
        SoftTFIDFJaroWinkler instance = new SoftTFIDFJaroWinkler();
        instance.prepare(ontClass, property);        
        
        String source = "Program Manager";
        String target = "Manager";
        double result = instance.compute(source, target);
        if(!Double.isNaN(result)){
            assertEquals(0.7, result, 0.05);
        }
        
    }    
}