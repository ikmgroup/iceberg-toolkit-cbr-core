/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.local.taxonomy;

import org.junit.Test;

/**
 *
 * @author sandro.emmenegger
 */
public class TaxonomyDistanceTest {
    
    public TaxonomyDistanceTest() {
    }

    /**
     * Test of compute method, of class TaxonomyDistance.
     */
    @Test
    public void testCompute() {
        
//        OntModel model = OntAO.getInstance().getOntModel();
//        OntClass rootClass = model.getOntClass(CBR_NS.CBR+"ApplicationService");
//        Individual appServiceQueryInstance = model.createIndividual(rootClass);
//        Individual appServiceCaseInstance = model.createIndividual(rootClass);
//        
//        //Taxonomy: ApplicationService(Class)-> <ConcreteService(Class)> -> Name(Label) -> VersionName(Property) -> VersionNumber (Property)
//        double maxDistanceSerie = (2.0*((1.0/2.0)+(1.0/3.0)+(1.0/4.0)))+1.0; 
//        //initalize propertyArray with amount of properties defined to be considered in the taxonomy.
//        SimResult[] taxonomicPropertiesSimilarityResults = new SimResult[3];
//
//        //equal instances without properties
//        TaxonomyDistance simFunction = new TaxonomyDistance();
//        double expected = 1.0;
//        double result = simFunction.compute(appServiceQueryInstance, appServiceCaseInstance, rootClass, taxonomicPropertiesSimilarityResults);
//        assertEquals("equal instances", expected, result, 0.00001);
        
        //target instances one level below, still without properties
//        OntClass dbmsClass = model.getOntClass(CBR_NS.EO+"DataBaseManagementSystem");
//        appServiceCaseInstance = model.createIndividual(dbmsClass);
//        expected = 1.0;
//        result = simFunction.compute(appServiceQueryInstance, appServiceCaseInstance, rootClass, taxonomicPropertiesSimilarityResults);
//        assertEquals("target with specific system", expected, result, 0.00001);
//        
//        //query: ERP, target: DBMS still without properties
//        OntClass erpClass = model.getOntClass(CBR_NS.EO+"EnterpriseResourcePlanningSystem");
//        appServiceQueryInstance = model.createIndividual(erpClass);
//        expected = 0.0; 
//        result = simFunction.compute(appServiceQueryInstance, appServiceCaseInstance, rootClass, taxonomicPropertiesSimilarityResults);
//        assertEquals("target with specific system", expected, result, 0.00001);        
        
         //equal instances with one property on second property level set, means the versionNumber (but still missing first level, the label)
//        appServiceQueryInstance = model.createIndividual(dbmsClass);
//        SimResult versionNameSimResult = new SimResult("elo:applicationServiceVersionName", "equals");
//        versionNameSimResult.setSourceNull(false);
//        versionNameSimResult.setTargetNull(false);
//        versionNameSimResult.setSimilarityValue(0.32);
//        taxonomicPropertiesSimilarityResults[1] = versionNameSimResult;
//        expected = 1.0;
//        result = simFunction.compute(appServiceQueryInstance, appServiceCaseInstance, rootClass, taxonomicPropertiesSimilarityResults);
//        assertEquals("first level property null, second level property set.", expected, result, 0.00001);
//        
//        //equal instances with one property set on case(target) instance
//        SimResult hasNameSimResult = new SimResult("rdf-schema:label", "equals");
//        hasNameSimResult.setSourceNull(true);
//        hasNameSimResult.setTargetNull(false);
//        taxonomicPropertiesSimilarityResults[0]= hasNameSimResult;
//        expected = 1.0;
//        result = simFunction.compute(appServiceQueryInstance, appServiceCaseInstance, rootClass, taxonomicPropertiesSimilarityResults);
//        assertEquals("1 case(target) property set, but first of query is null.", expected, result, 0.00001);
//        
//        //equal instances with one property set on query(source) instance
//        hasNameSimResult = new SimResult("rdf-schema:label", "equals");
//        hasNameSimResult.setSourceNull(false);
//        hasNameSimResult.setTargetNull(true);
//        taxonomicPropertiesSimilarityResults[0]= hasNameSimResult;
//        taxonomicPropertiesSimilarityResults[1]=null;
//        expected = 1.0-((0.5+(2*(1.0/3.0+1.0/4.0)))/maxDistanceSerie);  //0.47..
//        result = simFunction.compute(appServiceQueryInstance, appServiceCaseInstance, rootClass, taxonomicPropertiesSimilarityResults);
//        assertEquals("2 properties, but first of target is null.", expected, result, 0.00001);
//
//        //symetric test
//        double symetricResult = simFunction.compute(appServiceCaseInstance, appServiceQueryInstance, rootClass, taxonomicPropertiesSimilarityResults);
//        assertEquals("Expect function to be symetric.", result, symetricResult, 0.00001);        

    }
}