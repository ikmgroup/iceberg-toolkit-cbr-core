/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.global;

import ch.fhnw.cbr.model.sim.WeightedDouble;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sandro.emmenegger
 */
public class ProbabilisticTest {
    
    public ProbabilisticTest() {
    }

    /**
     * Test of computeLocalSimilarities method, of class Probabilistic with the following values:
     * 
     * similarity  | 	weight |  pow(similarity, weight) 
     * _______________________________________________________
     * 0.5         |     0.8   |  0.574349177 -> v1
     * 0.8         |     0.2   |  0.9563525   -> v2 
     * _______________________________________________________
     *      product (v1*v2):   |  0.549280272 (expected value)
     */
    @Test
    public void testCompute() {
        List<WeightedDouble> values = new ArrayList<>();
        values.add(new WeightedDouble(0.5, 0.8));
        values.add(new WeightedDouble(0.8, 0.2));
        boolean normalize = false;
        Probabilistic instance = new Probabilistic();
        double expResult = 0.549280272;
        double result = instance.computeLocalSimilarities(values, normalize);
        assertEquals(expResult, result, 0.000000001);
    }
}