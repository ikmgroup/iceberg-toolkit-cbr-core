/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model.sim.function.local;

import ch.fhnw.cbr.model.sim.function.LocalSimilarityFunction;
import ch.fhnw.cbr.model.sim.function.SimiliarityFunctionRegistry;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sandro.emmenegger
 */
public class VersionTest {
    
    /**
     * Test of compute method, of class Version.
     */
    @Test
    public void testCompute() {
        
        assertTrue(SimiliarityFunctionRegistry.containsLocalFunction("version"));
        LocalSimilarityFunction versionSimFunction = SimiliarityFunctionRegistry.getLocalFunction("version");

        assertEquals(1.0, versionSimFunction.compute("3", "3"), 0.001);
        assertEquals(1.0, versionSimFunction.compute("3.2-1", "3.2-1"), 0.001);
        assertEquals(1.0, versionSimFunction.compute("3.2 r1", "3.2 r1"), 0.001);
        
        assertEquals(0.75, versionSimFunction.compute("4", "3"), 0.001);
        assertEquals(0.75, versionSimFunction.compute("4", "3.4.1"), 0.001);
        assertEquals(0.999, versionSimFunction.compute("v4.5.1", "v3.4.1"), 0.001);
        
        assertEquals(1.0, versionSimFunction.compute("3.4", "3.4.1.r2"), 0.001);
        assertEquals(1.0, versionSimFunction.compute("   3.4", "3.4.1.r2"), 0.001);
        assertEquals(1.0, versionSimFunction.compute("   3.  4", "3. 4.1.r2"), 0.001);
        
        assertEquals(0.833, versionSimFunction.compute("3.4.1", "3.4.2"), 0.001);
    }
    
}
