/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.AbstractTest;
import ch.fhnw.cbr.model.sim.CaseMatch;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sandro.emmenegger
 */
public class CBRCaseViewTest extends AbstractTest {

    public CBRCaseViewTest() {
        Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).setLevel(Level.INFO);
        org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.ERROR);
    }

    @Test
    public void testGetInstances() {
        List<CBRCaseView> instances = CBRCaseView.getInstances();
        assertEquals(2, instances.size());
        assertNotNull("Expect test context available.", CBRCaseView.findCaseViewByURI(CASE_VIEW_PROJECT_STAFFING));
        assertNotNull("Expect test context available.", CBRCaseView.findCaseViewByURI(CASE_VIEW_THIRD_PARTY_SYSTEM_INTEGRATION));
    }

    @Test
    public void testGetConcernsAndRoles() {
        CBRCaseView context = CBRCaseView.findCaseViewByURI(CASE_VIEW_PROJECT_STAFFING);
        assertNotNull(context.getAddressedConcerns());
        assertTrue(context.getAddressedConcerns().size() > 0);

        for (CBRConcern concern : context.getAddressedConcerns()) {
            assertNotNull(concern.getBelongsToRoles());
            assertTrue(concern.getBelongsToRoles().size() > 0);
        }

    }

    /**
     * Compare query with junit testcase in casebase using different views resp.
     * case characterisations.
     */
    @Test
    public void testRetrieval() {
        OntModel model = OntAO.getInstance().getOntModel();

        //CBR Query case
        Individual queryInst = CASE_ONTCLASS.createIndividual(TEST_CBR_NS.CBR_UNITTEST + "QueryCaseInJUnitTest");

        //System
        Individual systemInst = model.createIndividual(model.getOntClass(TEST_CBR_NS.CBR_UNITTEST + "System"));
        systemInst.addLabel("Oracle", "EN");
        OntProperty systemName = model.getOntProperty(TEST_CBR_NS.CBR_UNITTEST + "systemHasVersionName");
        systemInst.addLiteral(systemName, "13c");
        OntProperty projCaseCharacterizedByModule = model.getOntProperty(TEST_CBR_NS.CBR_UNITTEST + "caseIsCharacterizedBySystem");
        queryInst.addProperty(projCaseCharacterizedByModule, systemInst);
        
        //Testmanager
        Individual testmanagerRole = model.getIndividual(TEST_CBR_NS.CBR_UNITTEST + "TestManagerRole");
        OntProperty businessActorHasAssignedBusinessRole = model.getOntProperty(TEST_CBR_NS.CBR_UNITTEST + "businessActorHasAssignedBusinessRole");
        Individual pseudoQueryBusinessActor = model.createIndividual(model.getOntClass(TEST_CBR_NS.CBR_UNITTEST + "Person"));
        pseudoQueryBusinessActor.addProperty(businessActorHasAssignedBusinessRole, testmanagerRole);
        OntProperty caseIsCharacterizedByInvolvedBusinessActor = model.getOntProperty(TEST_CBR_NS.CBR_UNITTEST + "caseIsCharacterizedByInvolvedBusinessActor");
        queryInst.addProperty(caseIsCharacterizedByInvolvedBusinessActor, pseudoQueryBusinessActor);
                
        CBRCaseView context = CBRCaseView.findCaseViewByURI(CASE_VIEW_PROJECT_STAFFING);
        List<CaseMatch> matchingCases = context.retreive(queryInst);
        System.out.println("___________________  Context: " + context.getLocalName() + " ___________________________________");
        for (CaseMatch caseMatch : matchingCases) {
            System.out.println("Case: " + caseMatch.getLabel() + " *****************");
            System.out.println(caseMatch.getSimilarityResult().toString());
            if (TEST_CASE_1.equals(caseMatch.getCaseURI())) {
                assertEquals(0.91d, caseMatch.getSimilarityResult().getSimilarityValue(), 0.01);
            }
        }

        context = CBRCaseView.findCaseViewByURI(CASE_VIEW_THIRD_PARTY_SYSTEM_INTEGRATION);
        matchingCases = context.retreive(queryInst);
        System.out.println("___________________  Context: " + context.getLocalName() + " ___________________________________");
        for (CaseMatch caseMatch : matchingCases) {
            System.out.println("Case: " + caseMatch.getLabel() + " *****************");
            System.out.println(caseMatch.getSimilarityResult().toString());
            if (TEST_CASE_1.equals(caseMatch.getCaseURI())) {
                assertEquals(0.76d, caseMatch.getSimilarityResult().getSimilarityValue(), 0.01);
            }
        }
    }

    /**
     * Compare query with nodes on the characterisation path without values set,
     * but with nodes at the end of the characterisation path with values set.
     * Ex. Only the role of a business actor is set in the query, but no
     * explicit business actor assigned as an expert to a system.
     *
     */
    @Test
    public void testRetrievalWithEmptyNodes() {
        OntModel model = OntAO.getInstance().getOntModel();

        //CBR Query case
        Individual queryInst = CASE_ONTCLASS.createIndividual(TEST_CBR_NS.CBR_UNITTEST + "QueryCaseInJUnitTest");

        //System
        Individual systemInst = model.createIndividual(model.getOntClass(TEST_CBR_NS.CBR_UNITTEST + "System"));
        OntProperty projCaseCharacterizedByModule = model.getOntProperty(TEST_CBR_NS.CBR_UNITTEST + "caseIsCharacterizedBySystem");
        queryInst.addProperty(projCaseCharacterizedByModule, systemInst);

        //Expert
        Individual systemExpertInst = model.createIndividual(model.getOntClass(TEST_CBR_NS.CBR_UNITTEST + "Person"));
        OntProperty systemExpertProperty = model.getOntProperty(TEST_CBR_NS.CBR_UNITTEST + "systemHasExpert");
        systemInst.addProperty(systemExpertProperty, systemExpertInst);
        //Role
        Individual expertBusinessRoleInst = model.createIndividual(model.getOntClass(TEST_CBR_NS.CBR_UNITTEST + "BusinessRole"));
        expertBusinessRoleInst.addLabel("Software Architect", "EN");
        OntProperty expertBusinessRoleProperty = model.getOntProperty(TEST_CBR_NS.CBR_UNITTEST + "businessActorHasAssignedBusinessRole");
        systemExpertInst.addProperty(expertBusinessRoleProperty, expertBusinessRoleInst);

        CBRCaseView context = CBRCaseView.findCaseViewByURI(CASE_VIEW_PROJECT_STAFFING);
        List<CaseMatch> matchingCases = context.retreive(queryInst);
        System.out.println("___________________  Context: " + context.getLocalName() + " ___________________________________");
        for (CaseMatch caseMatch : matchingCases) {
            System.out.println("Case: " + caseMatch.getLabel() + " *****************");
            System.out.println(caseMatch.getSimilarityResult().toString());

        }

    }

    /**
     * Test comparsion of roles based on taxonomy function.
     */
    @Test
    public void testRetrievalTaxonomyFunction() {
        OntModel model = OntAO.getInstance().getOntModel();

        //CBR Query case
        Individual queryInst = CASE_ONTCLASS.createIndividual(TEST_CBR_NS.CBR_UNITTEST + "QueryCaseInJUnitTest");

        //Involved business actor with specific role
        Individual projectMember = model.createIndividual(model.getOntClass(TEST_CBR_NS.CBR_UNITTEST + "Person"));
        OntProperty involvedBusinessActor = model.getOntProperty(TEST_CBR_NS.CBR_UNITTEST + "caseIsCharacterizedByInvolvedBusinessActor");
        queryInst.addProperty(involvedBusinessActor, projectMember);
        //Role
        Individual expertBusinessRoleInst = model.getIndividual(TEST_CBR_NS.CBR_UNITTEST + "Tester");
        OntProperty expertBusinessRoleProperty = model.getOntProperty(TEST_CBR_NS.CBR_UNITTEST + "businessActorHasAssignedBusinessRole");
        projectMember.addProperty(expertBusinessRoleProperty, expertBusinessRoleInst);
        
        //City
        Individual city = model.getIndividual(TEST_CBR_NS.CBR_UNITTEST + "StGallen");
        OntProperty locationProperty = model.getOntProperty(TEST_CBR_NS.CBR_UNITTEST + "caseIsCharacterizedByLocation");
        queryInst.addProperty(locationProperty, city);        

        CBRCaseView context = CBRCaseView.findCaseViewByURI(CASE_VIEW_PROJECT_STAFFING);
        List<CaseMatch> matchingCases = context.retreive(queryInst);
        System.out.println("___________________  Context: " + context.getLocalName() + " ___________________________________");
        for (CaseMatch caseMatch : matchingCases) {
            System.out.println("Case: " + caseMatch.getLabel() + " *****************");
            System.out.println(caseMatch.getSimilarityResult().toString());

            //TODO Test role comparsion with taxonomy function
        }

    }
}
