/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import ch.fhnw.cbr.AbstractTest;
import ch.fhnw.cbr.model.sim.function.GlobalSimilarityFunction;
import ch.fhnw.cbr.model.sim.function.SimiliarityFunctionRegistry;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.rdf.model.Model;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sandro.emmenegger
 */
public class CBRCaseTest extends AbstractTest{
    
    /**
     * Test of getCaseCharacterisationModel method, of class CBRCase.
     */
    @Test
    public void testCaseStates(){
        List<CBRCaseState> states = CBRCaseState.getAllCaseStates();
        assertNotNull(states);
        assertTrue(states.size() == 3);
        
        Individual testCase = OntAO.getInstance().getOntModel().getIndividual(TEST_CASE_1);
        assertNotNull(testCase);
        CBRCaseState state = CBRCase.getCaseState(testCase);

        assertNotNull(state);
        assertTrue("Learned case".equalsIgnoreCase(state.getLabel("EN")));
    }
    
    
    /**
     * Test of getCaseCharacterisationModel method, of class CBRCase.
     */
//    @Ignore
    @Test
    public void testGetCaseCharacterisationModel() {
        List<CBRCaseView> contexts = CBRCaseView.getInstances();
        assertTrue(contexts.size() > 0);
        CBRCaseView context = contexts.get(0);
        Model m = CBRCase.getCaseCharacterisationModel(context);
        assertNotNull(m);
        //m.write(System.out, "N3");    
    }
    
    /**
     * Test initialization of the case similarity model based on a selected viewpoint.
     */

//    @Ignore
    @Test
    public void testCBRCaseInit(){
        CBRCaseView caseView = CBRCaseView.findCaseViewByURI(CASE_VIEW_PROJECT_STAFFING);
        CBRCase ccCase = new CBRCase(caseView);
        GlobalSimilarityFunction expectedSimClass = SimiliarityFunctionRegistry.getGlobalFunction("probabilistic");
        String exptectedFunctionName = expectedSimClass.getClass().getName();
        String functionName = ccCase.getGlobalSimilarityFunction().getClass().getName();
        assertEquals("Expect probabilistic function set on case class for given view (project staffing). ", exptectedFunctionName, functionName);
     
        
        caseView = CBRCaseView.findCaseViewByURI(CASE_VIEW_THIRD_PARTY_SYSTEM_INTEGRATION);
        ccCase = new CBRCase(caseView);
        expectedSimClass = SimiliarityFunctionRegistry.getGlobalFunction("average");
        exptectedFunctionName = expectedSimClass.getClass().getName();
        functionName = ccCase.getGlobalSimilarityFunction().getClass().getName();
        assertEquals("Expect default, since no class function is set for the given view. ", exptectedFunctionName, functionName);
    }


}