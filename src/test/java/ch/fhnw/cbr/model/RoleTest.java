/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.model;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sandro.emmenegger
 */
public class RoleTest {
    
    /**
     * Test of getAllRoles method, of class CBRRole.
     */
    @Test
    public void testGetAllBusinessRoles() {
        
        String projectLeadRoleURI = TEST_CBR_NS.CBR_UNITTEST+"ProjectLeadRole";
        String expectedRoleGerman = "Projektleiter";
        String expectedRoleEnglish = "Project Lead";
        
        List<CBRRole> roles = CBRRole.getAllRoles();
//        roles.stream().forEach((role) -> System.out.println("Rolle: "+role.getLabel(CBRConfig.LANGUAGE)));
        CBRRole projectLeadRole = null;
        for (CBRRole role : roles) {
            if (projectLeadRoleURI.equals(role.getIndividual().getURI())){
                projectLeadRole = role;
                break;
            }
        }
        assertNotNull(projectLeadRole);
        assertEquals(expectedRoleGerman, projectLeadRole.getLabel("DE"));
        assertEquals(expectedRoleEnglish, projectLeadRole.getLabel("EN"));
        
    }
    
}
