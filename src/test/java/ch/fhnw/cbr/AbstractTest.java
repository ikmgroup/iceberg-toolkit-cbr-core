/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr;

import ch.fhnw.cbr.model.CBR_NS;
import ch.fhnw.cbr.persistence.OntAO;
import com.hp.hpl.jena.ontology.OntClass;

/**
 * Base class for component tests.
 * 
 * @author sandro.emmenegger
 */
public abstract class AbstractTest {
    
    public static final String CASE_VIEW_PROJECT_STAFFING = "http://ikm-group.ch/cbr/cbr-unittest#ProjectStaffing_CaseView";
    public static final String CASE_VIEW_THIRD_PARTY_SYSTEM_INTEGRATION = "http://ikm-group.ch/cbr/cbr-unittest#ThirdPartySystemIntegration_CaseView";
    
    public static final OntClass CASE_ONTCLASS = OntAO.getInstance().getOntClass(CBR_NS.CBR+"Case");
    
    public static final String TEST_CASE_1 = "http://ikm-group.ch/cbr/cbr-unittest#UnitTestCase_1";

}
