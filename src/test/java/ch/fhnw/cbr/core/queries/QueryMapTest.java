/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.core.queries;

import ch.fhnw.cbr.core.config.CBR;
import java.io.InputStream;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sandro.emmenegger
 */
public class QueryMapTest {
    
    public QueryMapTest() {
    } 

    /**
     * Test of getQuery method, of class QueryMap.
     */
    @Test
    public void testGetQuery() {
        String queriesFile = CBR.CONFIG.getString("queries.file");
        assertNotNull(queriesFile);
        InputStream in = QueryMap.class.getResourceAsStream(queriesFile);
        assertNotNull("queries file not found.", in);
        
        String query = QueryMap.getQuery("getLiteralProperties");
        assertNotNull(query);
        assertTrue(query.length() > 0);
    }
    
}
