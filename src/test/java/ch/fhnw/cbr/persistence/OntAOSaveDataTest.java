/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.persistence;

import ch.fhnw.cbr.AbstractTest;
import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.model.TEST_CBR_NS;
import com.hp.hpl.jena.ontology.Individual;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author sandro.emmenegger
 */
public class OntAOSaveDataTest extends AbstractTest {
    
    private String originalDataFilePropertyValue;
    private String testDataFilePropertyValue;
    
    @Before
    public void init() throws IOException{
       originalDataFilePropertyValue = CBR.CONFIG.getString("ontology.data.file");
       File originalFile = new File(originalDataFilePropertyValue);
       assertTrue(originalFile.exists());
       testDataFilePropertyValue = originalDataFilePropertyValue.replace(".ttl", "_temp.ttl");
       File testFile = new File(testDataFilePropertyValue);
       Files.copy(originalFile.toPath(), testFile.toPath());
       assertTrue(testFile.exists());
       CBR.CONFIG.setProperty("ontology.data.file", testDataFilePropertyValue);
    }

    @Test
    public void testSaveDataToFile() throws Exception {
        
        String testInstanceURI = TEST_CBR_NS.CBR_UNITTEST + "CaseInstanceToSaveTest";
        Individual caseInstance = CASE_ONTCLASS.createIndividual(testInstanceURI);
        assertNotNull(OntAO.getInstance().getOntDataModel().getIndividual(testInstanceURI));
        
        File testFile = new File(testDataFilePropertyValue);
        String content = new String(Files.readAllBytes(testFile.toPath()));
        assertTrue(!content.contains(testInstanceURI));

        OntAO.getInstance().persistData();
        
        content = new String(Files.readAllBytes(testFile.toPath()));
        assertTrue(content.contains(caseInstance.getLocalName()));  
    }
    
    @After
    public void cleanUp(){
        File testFile = new File(testDataFilePropertyValue); 
        if(testFile.exists()){
            testFile.delete();
        }
        CBR.CONFIG.setProperty("ontology.data.file", originalDataFilePropertyValue);
    }
}
