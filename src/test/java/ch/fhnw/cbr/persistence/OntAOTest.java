/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.fhnw.cbr.persistence;

import ch.fhnw.cbr.AbstractTest;
import ch.fhnw.cbr.core.config.CBR;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sandro.emmenegger
 */
public class OntAOTest extends AbstractTest {
    
    public OntAOTest() {
    }
    
    @Test
    public void testInit(){
        OntModel schemaModel = OntAO.getInstance().getOntSchemaModel();
        assertNotNull(schemaModel);
        assertTrue(schemaModel.listClasses().hasNext());
        
        OntModel model = OntAO.getInstance().getOntModel();
        assertNotNull(model);
        assertTrue(model.listClasses().hasNext());
        
        Individual testView = model.getIndividual(CASE_VIEW_PROJECT_STAFFING);
        assertNotNull(testView);
        
        OntModel dataModel = OntAO.getInstance().getOntDataModel();
        assertNotNull(dataModel);
        assertFalse(dataModel.listClasses().hasNext());
    }

    @Test
    public void testRefreshSingleton() throws ConfigurationException {
        assertNotNull(CBR.CONFIG);
        assertEquals("", CBR.CONFIG.getString("addtional", ""));
        
        Configuration newConfiguration = new PropertiesConfiguration("config_test.properties");
        OntAO.getInstance().refreshSingleton(newConfiguration);

        assertEquals("Test", CBR.CONFIG.getString("addtional", ""));

    }
    
}
